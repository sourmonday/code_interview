
from collections import defaultdict
from math import floor, sqrt


def number_swap(num1, num2):
    num2 -= num1  # num2 = num2 - num1
    num1 += num2  # num1 = num1 + (orig_num2 - orig_num1) = orig_num2

    num2 -= num1  # (orig_num2 - orig_num1) - orig_num2
    num2 *= -1
    return num1, num2


class WordFrequency:
    def __init__(self):
        self.cached_result = None
        self.cached_strstream = None

    def word_freq_firstprinc_multiple(self, fstream):
        passage = self._build_passage(fstream)

        if passage == self.cached_strstream:
            return self.cached_result

        self.cached_result = defaultdict(int)
        self.cached_strstream = passage

        words = self.cached_strstream.split()
        for word in words:
            self.cached_result[word] += 1

        return self.cached_result

    @staticmethod
    def _build_passage(fstream) -> str:
        if type(fstream) is str:
            return fstream
        return fstream.read()


class Point:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y


class Line:
    def __init__(self, origin: Point, destination: Point):
        self.origin = origin
        self.destination = destination

    def __len__(self):
        x_delta = self.destination.x - self.origin.x
        y_delta = self.destination.y - self.origin.y
        return self._hypotenuse(x_delta, y_delta)

    @staticmethod
    def _hypotenuse(x, y):
        return sqrt(x*x + y*y)


class Intersection:
    def __init__(self, line1: Line, line2: Line):
        self.line1 = line1
        self.line2 = line2

    def get_intersection(self):
        if not self._has_intersection():
            return None

    def _has_intersection(self) -> bool:
        return self._x_intersects() and self._y_intersects()

    def _x_intersects(self) -> bool:
        return self.line1.origin.x <= self.line2.origin.x <= self.line1.destination.x or \
               self.line1.origin.x <= self.line2.destination.x <= self.line1.destination.x

    def _y_intersects(self) -> bool:
        return self.line1.origin.y <= self.line2.origin.y <= self.line1.destination.y or \
               self.line1.origin.y <= self.line2.destination.y <= self.line1.destination.y


class FactorialZero:
    _factors_of_ten = {2, 5, 10}

    def __init__(self, fact):
        self.fact = fact

    def trailing_zeros(self) -> int:
        tens = self._tens_factor_primefactors(self.fact)
        return tens[10] + min(tens[2], tens[5])

    @staticmethod
    def _tens_factor_primefactors(number) -> defaultdict:
        prime_factor = FactorialZero._prime_factorization(number)
        tens_dict = defaultdict(int)

        for factor in FactorialZero._factors_of_ten:
            tens_dict[factor] = prime_factor[factor]

        return tens_dict

    @staticmethod
    def _prime_factorization(number) -> defaultdict:
        prime_factors = defaultdict(int)

        while number % 2 == 0:
            prime_factors[2] += 1
            number /= 2

        for x in range(3, floor(sqrt(number)) + 1, 2):
            while number % x == 0:
                prime_factors[x] += 1
                number /= x

        return prime_factors


class Operations:
    """For some reason, the existence of multiply, subtraction, and division has disappeared. Everything must occur
    with only addition"""
    @staticmethod
    def multiply(a, b):
        result = 0
        for _ in range(b):
            result += a
        return result

    @staticmethod
    def subtract(a, b):
        return a + Operations.invert_sign(b)

    @staticmethod
    def invert_sign(a):
        return -1 * a  # replace with bit flip

    @staticmethod
    def divide(a, b):
        if b == 0:
            raise ZeroDivisionError
        pass


class LivingPeople:
    """
    List of people with birth and death years, implement a method to compute the year with the most number of people alive.
    All people in this list are born in between 1900 and 2000, inclusive.

    (BIRTH_YEAR, DEATH_YEAR)

    Approach 1. For person in person_list: for year in range(birth_year, death_year + 1), alive[year]++
    O(P * Y), Naive approach

    Approach 2. Sort by Death Year
    (???, 2000)
    (???, 1999)

    for year in range(period_start=2000, period_end=1899, step=-1)
    alive[year=2000] = P(death years >= 2000)
    alive[year=1999] = P - P(born_year == 2000) - P(death_year > year(1999)

    Approach 3. Stable Sort people
    First sort by Death year, then by Birth

    for year in range(period_start=1900, period_end_2001)
    alive[year=1900] = P(born @ 1900)
    alive[year=1901] = alive[year-1(1900)] - P(died year-1) + P(born @ 1901)
    alive[year=1902] = alive[year-1(1901)] - P(died year-1) + P(born @ 1902)

    O(P + Y)
    born:dict [year] - 1st pass
    died:dict [year] - 1st pass
    """
    def __init__(self, ListOfRecords: list):
        self.iter = iter(ListOfRecords)

    def calc_heaviest_pop_year(self):
        born, died = self._build_birth_death_recs(self.iter)
        alive = defaultdict(int)
        population, pop_year = -1, None

        for year in range(1900, 2001):
            alive[year] = alive[year - 1] - died[year - 1] + born[year]
            if alive[year] > population:
                population = alive[year]
                pop_year = year

        return pop_year

    @staticmethod
    def _build_birth_death_recs(iterator: iter) -> tuple:
        born = defaultdict(int)
        died = defaultdict(int)

        for born_year, death_year in iterator:
            born[born_year] += 1

            if death_year >= 2000:
                died[2000] += 1
            else:
                died[death_year] += 1

        return born, died


class PlankFactory:
    class Plank:
        def __init__(self, length):
            self.length = length

        def __len__(self):
            return self.length

    LONGER = 10
    SHORTER = 5

    @classmethod
    def Longer(cls):
        return cls.Plank(cls.LONGER)

    @classmethod
    def Shorter(cls):
        return cls.Plank(cls.SHORTER)


class DivingBoard:
    """You are building a diving board by placing a bunch of planks of wood end-to-end. There are two types of planks,
    one of length /shorter/ and one of length /longer/. You must use exactly K planks of wood. Write a method to
    generate all possible lengths for the diving board.

    This is like having a K-bit value. If K were 8, for example, the wood used could be represented by an 8-bit value.
    10010011, for example, would be 4 (1's), i.e., /longer/plank and 4(0's), i.e., /shorter/plank. You would then
    calculate the length with X * len(shorter) + Y * len(longer)

    Duplicates make the same length"""

    def __init__(self, plank_count):
        self.count = plank_count

    def generate_possible_lengths(self) -> set:
        lengths = set()

        for short_count in range(self.count + 1):
            short_totals = short_count * len(PlankFactory.Shorter())
            long_totals = (self.count - short_count) * len(PlankFactory.Longer())
            lengths.add(short_totals + long_totals)

        return lengths


if __name__ == '__main__':
    board = DivingBoard(8)
    print(board.generate_possible_lengths())
