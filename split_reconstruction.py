
"""
# Problem 22

Given a dictionary of words and a string made up of those words without spaces, return the original sentence in a list.
If there is more than one possible reconstruction, return any of them. If no possible reconstruction, then return NULL.

As you run through that string, you can detect words in the dictionary if you convert that dictionary to a Trie. Once
a word is detected, however, that is where a decision must be made to either continue with the word to see if it is
part of a longer word or to start the next letter as part of a new word.

Some considerations, if the next letter in the string-stream is not a valid continuation, the next letter that follows
must be the start of a new word.

If the next letter is instead not a valid word, then it must be that the next character is a continuation to a longer
word.

Solve this using backtracking to the last valid path every time the second substring doesn't work.
"""

from collections import defaultdict
from collections import Iterable



class Node:
    def __init__(self, value):
        self.value = value
        self.values_adjacency = set()
        self.nodes_adjacency = set()
        self.values_to_nodes = defaultdict(set)

    def attach_node_neighbor(self, node: 'Node'):
        self.nodes_adjacency.add(node)
        self.values_adjacency.add(node.value)
        self.values_to_nodes[node.value].add(node)

    def attach_node_value(self, value):
        self.values_adjacency.add(value)
        new_node = Node(value)
        self.nodes_adjacency.add(new_node)
        self.values_to_nodes[value].add(new_node)

    def get_nodes_with_value(self, value):
        return self.values_to_nodes[value]

    def __repr__(self):
        return repr(self.value)


END_OF_WORD_NODE = Node(None)


class Trie:
    def __init__(self, iterable = None):
        self.root = Node(None)
        if iterable:
            assert isinstance(iterable, Iterable)
            for word in iterable:
                self.insert_word(word)

    def insert_word(self, string: str):
        current_node = self.root
        for letter in string:
            if letter not in current_node.values_adjacency:
                current_node.attach_node_value(letter)
            current_node = self.get_node_with_letter(current_node, letter)

        current_node.attach_node_neighbor(END_OF_WORD_NODE)

    def matches(self, string):
        letter_path = list()
        current_node = self.root

        for index, letter in enumerate(string):
            current_node = self.get_node_with_letter(current_node, letter)
            if not current_node:
                break

            letter_path.append(letter)
            if self.letter_constitutes_end_of_word(current_node):
                yield (index, ''.join(letter_path))

            if index != (len(string) - 1) and not string[index + 1] in current_node.values_adjacency:
                break

    @staticmethod
    def get_node_with_letter(current_node, letter):
        letter_node_set = current_node.get_nodes_with_value(letter).copy()
        if len(letter_node_set) is 1:
            return letter_node_set.pop()
        return None

    @staticmethod
    def letter_constitutes_end_of_word(current_node: Node) -> bool:
        return None in current_node.values_adjacency if current_node else False


class SplitReconstruction:
    def __init__(self, target: str, dictionary: Trie):
        self.target = target
        self.dictionary = dictionary

    def solve(self):
        return self.solve_string(self.target)

    def solve_string(self, string):
        if not string:
            return ''

        for index, next_match in self.dictionary.matches(string):
            solution = self.solve_string(string[index + 1:])
            if solution is not None:
                return next_match + ' ' + solution

        return None



"""
I want to go through the Trie and detect the first available word, go through it depth-first.

On the first-avail word, branch in and solve as solution_word + get_solution_word(substr[x:]) Return all the way if 
found.

Else, backtrack, depth-first and try until solution found. If no available solution, return None
"""


if __name__ == '__main__':
    given_set = {"the", "quick", "brown", "bat", "fox", "bed", "bath", "bedbath", "and", "beyond"}
    dictionary = Trie(given_set)

    target_string = "bedbathandbeyond"
    trial = SplitReconstruction(target_string, dictionary)
    solution = trial.solve()
    print(solution)
