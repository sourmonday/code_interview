
class SetOfStacks:
    def __init__(self, stack_max):
        self.stack_list = list()
        self.stack_list.append(list())
        self.stack_max = stack_max
        self.current_stack_index = 0
        self.largest_stack_index = 0

    def set_stack_max(self, new_max):
        self.stack_max = new_max

    def pop_at(self, index):
        self.current_stack_index = index
        self._fix_current_index_if_overflow()

        pop_value = self.pop()

        self.current_stack_index = self.largest_stack_index
        return pop_value

    def pop(self):
        if not self.current_stack_empty():
            return self.get_current_stack().pop()

        self._decommission_stack_at(self.current_stack_index)
        return self.pop()

    def _decommission_stack_at(self, stack_index):
        self.stack_list.pop(stack_index)
        self.largest_stack_index -= 1

        self._fix_current_index_if_overflow()

    def _fix_current_index_if_overflow(self):
        if self.largest_stack_index < self.current_stack_index:
            self.current_stack_index = self.largest_stack_index

    def push(self, element):
        if self.current_stack_has_space():
            self.get_current_stack().append(element)
        else:
            self.create_new_stack_with(element)

    def create_new_stack_with(self, element):
        self.stack_list.append([element])
        self.current_stack_index += 1

    def get_current_stack(self):
        return self.stack_list[self.current_stack_index]

    def current_stack_empty(self):
        return not self.get_current_stack()

    def current_stack_has_space(self):
        return len(self.get_current_stack()) < self.stack_max

    def __repr__(self):
        return ' . '.join(map(str, self.stack_list))


if __name__ == '__main__':
    stackSet = SetOfStacks(5)

    for _ in range(7):
        stackSet.push(1)

    stackSet.pop_at(0)
    stackSet.pop()

    print(stackSet)
