
from abc import ABC, abstractmethod

from data_structs.binary_trees import AVLTree, BaseBinaryTree, BinaryTreeKit
from data_structs.exceptions import BalancedTreeTypeNotImplemented
from data_structs.nodes import AVLNode, TreeNode
from data_structs.stack_queue import Stack


class ValidationKit:
    def __init__(self, tree):
        self.tree = tree

    def binary_search_property(self):
        assert isinstance(self.tree, BaseBinaryTree)
        return BinarySearchValidator(self.tree.root)

    def balanced_tree_property(self):
        if isinstance(self.tree, AVLTree):
            return AVLBalancedValidator(self.tree.root)
        raise BalancedTreeTypeNotImplemented


class ValidationWorker(ABC):
    def __init__(self, start_node: TreeNode):
        self.head = start_node

    @abstractmethod
    def validate_property(self):
        raise NotImplementedError


class BinarySearchValidator(ValidationWorker):
    def validate_property(self):
        return self.validate_subtree(self.head)

    def validate_subtree(self, node) -> bool:
        return self._run_in_order_validation_from_subtree_root(node)

    @staticmethod
    def _run_in_order_validation_from_subtree_root(root: TreeNode) -> bool:
        inorder_run = list()
        processing_stk = Stack()
        current = root

        while processing_stk or current:
            if current:
                assert isinstance(current, TreeNode)
                processing_stk.push(current)
                current = current.get_left_child()
            else:
                current = processing_stk.pop()
                inorder_run.append(current)
                current = current.get_right_child()

        return inorder_run == sorted(inorder_run)


class AVLBalancedValidator(ValidationWorker):
    def validate_property(self):
        assert isinstance(self.head, AVLNode)
        return self.validate_subtree_is_balanced(self.head)

    def validate_subtree_is_balanced(self, node: AVLNode) -> bool:
        left_is_balanced, right_is_balanced = True, True
        if node.get_left_child():
            left_is_balanced = self.validate_subtree_is_balanced(node.get_left_child())
        if node.get_right_child():
            right_is_balanced = self.validate_subtree_is_balanced(node.get_right_child())

        node.update_avl_balance_and_height()

        return not node.is_unbalanced() and left_is_balanced and right_is_balanced


if __name__ == '__main__':
    t = BinaryTreeKit().avl_tree()

    t.insert(TreeNode(5))
    t.insert(TreeNode(2))
    t.insert(TreeNode(1))

    tv = ValidationKit(t).balanced_tree_property()
    assert tv.validate_property()

    tv = ValidationKit(t).binary_search_property()
    assert tv.validate_property()
    print(t)
