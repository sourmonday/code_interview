

class ManachersPalindrome:
    """
    Attempt at the alg after IDeserve's video on Youtube
    """
    INITIAL_START_VALUE = 2
    INITIAL_BOUNDARY_VALUES = 1

    def __init__(self, candidate):
        self.candidate = candidate
        self.candidate_len = len(self.candidate)
        self.info_cache = None

    def get_longest_substr_len(self):
        if self.candidate_len == 0:
            return 0

        self._build_delimited_candidate()
        longest_palindrome_substr = self._build_longest_substr_list(self.candidate_len)

        center_boundary, right_boundary = self.INITIAL_BOUNDARY_VALUES, self.INITIAL_BOUNDARY_VALUES

        for current_index in range(self.INITIAL_START_VALUE, self.candidate_len):
            index_record = (current_index, center_boundary, right_boundary)
            if self._index_in_boundary(index_record):
                mirror_value = self._find_mirror_value(longest_palindrome_substr, index_record)
                distance_to_exit_boundary = right_boundary - current_index
                expand_from_distance = min(mirror_value, distance_to_exit_boundary)
            else:
                expand_from_distance = 0

            longest_palindrome_substr[current_index] = self._calculate_longest_from_distance(current_index,
                                                                                             expand_from_distance)

            current_right_index = current_index + longest_palindrome_substr[current_index]

            if current_right_index > right_boundary:
                center_boundary = current_index
                right_boundary = current_right_index

        self._restore_values_from_cache()
        return max(longest_palindrome_substr)

    def _build_delimited_candidate(self):
        new_candidate = '#'.join(list(self.candidate))
        new_candidate = list(new_candidate)
        new_candidate.append('#')
        new_candidate.insert(0, '#')

        self.info_cache = (self.candidate, self.candidate_len)
        self.candidate = new_candidate
        self.candidate_len = len(self.candidate)

    def _build_longest_substr_list(self, length):
        substr_list = [0] * length
        substr_list[0], substr_list[1] = self._initial_substr_values()
        return substr_list

    @staticmethod
    def _initial_substr_values():
        return 0, 1

    @staticmethod
    def _index_in_boundary(index_record):
        current, center, right = index_record
        return center < current <= right

    @staticmethod
    def _find_mirror_value(distance_list, index_record):
        current, center, _ = index_record
        delta = current - center
        return distance_list[center - delta]

    def _calculate_longest_from_distance(self, center, starting_distance):
        longest = starting_distance

        while self._expand_once_around_center_from_distance(center, longest):
            longest += 1

        return longest

    def _expand_once_around_center_from_distance(self, center, starting_distance):
        new_right_boundary = center + starting_distance + 1
        new_left_boundary = center - starting_distance - 1

        if new_right_boundary >= self.candidate_len or new_left_boundary < 0:
            return False

        return self.candidate[new_left_boundary] == self.candidate[new_right_boundary]

    def _restore_values_from_cache(self):
        self.candidate, self.candidate_len = self.info_cache
        self.info_cache = None


if __name__ == '__main__':
    from test.TestPalindromeSubstring import PalindromeBuilder

    palindrome = PalindromeBuilder(10)
    # substr_checker = MalanchersPalindrome(palindrome.palindrome)
    substr_checker = ManachersPalindrome('abaaba')

    longest_len = substr_checker.get_longest_substr_len()

    print(longest_len)
