

def get_nth_right_bit(number, bit):
    if number & (1 << (bit - 1)) != 0:
        return 1
    return 0


def set_nth_right_bit(original, bit):
    return original | (1 << (bit - 1))


class AddWithoutPlus:
    """
    Add Without Plus: Write a function that adds two numbers. Do not use + or any arithmetic operators
    """
    def __init__(self, number1, number2):
        self.number1 = number1
        self.number2 = number2

    def add(self) -> int:
        result = 0
        carry_bit = 0

        for bit_number in range(1, 65):
            ones_bit = get_nth_right_bit(self.number1, bit_number)
            twos_bit = get_nth_right_bit(self.number2, bit_number)

            if self.odd_amount_true((ones_bit, twos_bit, carry_bit)):
                result = set_nth_right_bit(result, bit_number)

            if self.carry_bit_should_set((ones_bit, twos_bit, carry_bit)):
                carry_bit = 1
            else:
                carry_bit = 0

        return result

    # noinspection PyPep8
    @staticmethod
    def carry_bit_should_set(iterable):
        assert len(iterable) is 3

        filtered = filter(lambda x: x == True, iterable)
        return len(tuple(filtered)) > 1

    @staticmethod
    def odd_amount_true(iterable) -> bool:
        is_odd = False
        for item in iterable:
            if item:
                is_odd = not is_odd

        return is_odd
