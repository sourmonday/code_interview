

class EdgeAlreadyExistsError(Exception):
    pass


class Edge:
    def __init__(self, node1, node2, weight=1):
        self.endpoints = (node1, node2)
        self.weight = weight

    def __repr__(self):
        return '->'.join(map(str, (self.endpoints[0].value, self.endpoints[1].value)))


class DirectedGraphNode:
    def __init__(self, value):
        self.value = value
        self.adj_list = set()


class DirectedGraph:
    def __init__(self):
        self.nodes = set()
        self.directed_edges = dict()

    def get_node_with_value(self, value):
        for node in self.nodes:
            if node.value is value:
                return node
        return None

    def add_new_node_valued_as(self, value):
        self.nodes.add(DirectedGraphNode(value))

    def add_edge(self, node1, node2):
        if ((node1, node2) in self.nodes):
            raise EdgeAlreadyExistsError

        node1.adj_list.add(node2)
        self.nodes.add(node1)
        self.nodes.add(node2)

        edge = Edge(node1, node2)
        self.directed_edges[edge.endpoints] = edge

    def __repr__(self):
        repr_str = 'Node List:\n' + ' '.join(map(str, self.nodes)) + '\n\n'
        repr_str += str(self.directed_edges)
        return repr_str


if __name__ == '__main__':
    g = DirectedGraph()

    n1 = DirectedGraphNode(1)
    n2 = DirectedGraphNode(2)

    g.add_edge(n1, n2)

    print(g)
