
# Daily Coding Problem #3

"""
Given the root to a binary tree, implement serialize(root), which serializes the tree into a string, and
deserialize(s), which deserializes the string back into a tree
"""

class Queue(list):
    def enqueue(self, element):
        self.append(element)

    def dequeue(self):
        return self.pop(0)


class Node:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def parent_index(index: int) -> int:
    if index % 2:
        return index // 2 - 1
    return index // 2

def is_left(index: int) -> bool:
    return index % 2 == 1

def is_right(index: int) -> bool:
    return index % 2 == 0

def left_child_index(index: int) -> int:
    return 2 * index + 1

def right_child_index(index: int) -> int:
    return 2 * index + 2

def deserialize(serialization: str) -> Node:
    deserialize_list = serialization.split('+')

    for index, node in enumerate(deserialize_list):
        if deserialize_list[index] == 'NoNodeAtLocation':
            deserialize_list[index] = None
        elif deserialize_list[index] == 'NodeButNoValue':
            deserialize_list[index] = Node(None)
        else:
            deserialize_list[index] = Node(deserialize_list[index])

    for index, node in enumerate(deserialize_list):
        if deserialize_list[index]:
            deserialize_list[index].left = deserialize_list[left_child_index(index)]
            deserialize_list[index].right = deserialize_list[right_child_index(index)]

    return deserialize_list[0]

def serialize(root: Node) -> str:
    # if you look at how the heap organizes nodes into an array, we can use that for our serialization.
    # You can serialize the tree into an array (list) using a Queue
    # This assumes that the symbol uses as a delimter isn't used in the value, for more robustness, implement some
    # hashing scheme where you remove symbols from an allowable set and use one within the set that remains or you
    # just disallow particular symbols. For now, I'll use the magic symbol
    if not root:
        raise EOFError

    queue = Queue()
    queue.enqueue(root)
    serialized_list = list()

    while queue:
        current_node = queue.dequeue()
        if not current_node:
            serialized_list.append('NoNodeAtLocation')
            continue

        if not current_node.val:
            serialized_list.append('NodeButNoValue')
        else:
            serialized_list.append(current_node.val)

        queue.enqueue(current_node.left)
        queue.enqueue(current_node.right)

    return '+'.join(serialized_list)


if __name__ == '__main__':
    node = Node('root', Node('left', Node('left.left')), Node('right'))

    assert isinstance(serialize(node), str)
    print(serialize(node))
    assert deserialize(serialize(node)).left.left.val == 'left.left'
