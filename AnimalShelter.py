
from LinkedList import LinkedListQueue


class Animal:
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return self.name


class Dog(Animal):
    pass


class Cat(Animal):
    pass


class AnimalShelter:
    def __init__(self):
        self.shelter_queue = LinkedListQueue()
        self.dog_queue = LinkedListQueue()
        self.cat_queue = LinkedListQueue()

        self.queue_dispatch = {Dog: self.dog_queue, Cat: self.cat_queue}

    def dequeue_any(self):
        while not self._is_shelter_head_correct():
            self.shelter_queue.dequeue()

        self.queue_dispatch[self._animal_type_of_head()].dequeue()
        return self.shelter_queue.dequeue()

    def dequeue_dog(self):
        adoptee = self.queue_dispatch[Dog].dequeue()
        return adoptee

    def dequeue_cat(self):
        adoptee = self.queue_dispatch[Cat].dequeue()
        return adoptee

    def _is_shelter_head_correct(self):
        if not self.queue_dispatch[self._animal_type_of_head()]:
            return False
        return self.shelter_queue.head == self.queue_dispatch[self._animal_type_of_head()].head

    def _animal_type_of_head(self):
        return type(self.shelter_queue.head.value)

    def enqueue(self, animal):
        self._enqueue_in_both_queues(self.shelter_queue, self.queue_dispatch[type(animal)], animal)

    @staticmethod
    def _enqueue_in_both_queues(queue1: LinkedListQueue, queue2: LinkedListQueue, item):
        queue1.enqueue(item)
        queue2.enqueue(item)

    def __repr__(self):
        all_queues = list(self.queue_dispatch.values())
        all_queues.append(self.shelter_queue)
        return '\n'.join(map(str, all_queues))


if __name__ == '__main__':
    animal_listing = [Dog("rover"), Dog("spot"), Cat("simba"), Dog("chuck")]
    shelter = AnimalShelter()

    for animal in animal_listing:
        shelter.enqueue(animal=animal)

    print(shelter)

    shelter.dequeue_any()
    shelter.dequeue_cat()
    shelter.dequeue_any()
    shelter.dequeue_any()

    print(shelter)
