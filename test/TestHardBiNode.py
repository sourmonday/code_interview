
import unittest

from random import randint
from data_structs.binary_trees import BinaryTreeKit
from data_structs.nodes import BinaryNodeKit

import HardBiNode

class TestHardBiNode(unittest.TestCase):
    @staticmethod
    def generate_tree(size):
        tree = BinaryTreeKit.base_tree()
        uniques = set()
        for _ in range(size):
            node_value = randint(0, 10000)
            while node_value in uniques:
                node_value = randint(0, 10000)
            uniques.add(node_value)
            tree.insert(BinaryNodeKit.base_node(node_value))

        return tree, list(uniques)

    def test_small_expected_tree(self):
        static_tree = BinaryTreeKit.base_tree()
        static_tree.insert(BinaryNodeKit.base_node(2))
        static_tree.insert(BinaryNodeKit.base_node(3))
        static_tree.insert(BinaryNodeKit.base_node(1))

        in_order_linked_list = HardBiNode.HardBiNode(static_tree).convert_to_double_linked_list()
        self.assertEqual([1,2,3], in_order_linked_list.order_repr())

    def test_generated_trees(self):
        for _ in range(1000):
            generated, expected = self.generate_tree(randint(0, 100))
            in_order_linked_list = HardBiNode.HardBiNode(generated).convert_to_double_linked_list()

            expected.sort()
            self.assertEqual(expected, in_order_linked_list.order_repr())
