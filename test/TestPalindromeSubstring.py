
from collections import deque

import random
import string
import unittest

from Palindrome import ManachersPalindrome


class PalindromeBuilder:
    def __init__(self, size):
        self.size = size
        self.palindrome = self._generate_palindrome()

    def _generate_palindrome(self):
        palindrome = deque()

        if self._needs_mid_char():
            palindrome.append(self._random_char())

        for _ in range(self.size // 2):
            self._sandwich_with_letter(self._random_char(), palindrome)

        return ''.join(palindrome)

    def _needs_mid_char(self):
        return self.size % 2 == 1

    @staticmethod
    def _sandwich_with_letter(letter, sequence: deque):
        sequence.append(letter)
        sequence.appendleft(letter)

    @staticmethod
    def _random_char():
        return random.choice(string.ascii_letters)

    def __repr__(self):
        return self.palindrome


class TestPalindromeSubstr(unittest.TestCase):
    def setUp(self):
        p_build = list()
        for _ in range(10):
            new_pal = next(self._palindrome())
            p_build.append(new_pal)

        self.longest_substr_answer = self._get_longest_substr_answer(p_build)
        self.has_substr_string = ''.join(p_build)
        self.no_substr_string = string.ascii_letters * 50
        self.entire_string = next(self._palindrome())

    @staticmethod
    def _get_longest_substr_answer(p_iterable):
        max_len = 0
        for item in p_iterable:
            max_len = max(max_len, len(item))

        return max_len

    @staticmethod
    def _palindrome():
        while True:
            psize = random.randint(0, 1000)
            p = PalindromeBuilder(psize)
            yield p.palindrome

    def test_empty_string(self):
        checker = ManachersPalindrome("")
        self.assertEqual(0, checker.get_longest_substr_len())

    def test_no_substr(self):
        checker = ManachersPalindrome(self.no_substr_string)
        self.assertEqual(1, checker.get_longest_substr_len())

    def test_entire_string(self):
        checker = ManachersPalindrome(self.entire_string)
        self.assertEqual(len(self.entire_string), checker.get_longest_substr_len())

    def test_random_size_substr(self):
        checker = ManachersPalindrome(self.has_substr_string)
        self.assertEqual(self.longest_substr_answer, checker.get_longest_substr_len())

    def test_small_string(self):
        MAGIC_STRING = 'abccbdeffedbccbagh'
        checker = ManachersPalindrome(MAGIC_STRING)
        self.assertEqual(len(MAGIC_STRING) - 2, checker.get_longest_substr_len())
