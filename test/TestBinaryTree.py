
import unittest

from TreeValidator import ValidationKit
from data_structs.nodes import TreeNode
from data_structs.binary_trees import BinaryTreeKit


class TestBinaryTreeValidator(unittest.TestCase):
    @staticmethod
    def _build_validator_with_treeroot(head: TreeNode):
        tree_to_validate = BinaryTreeKit().base_tree()
        tree_to_validate.set_root(head)
        return ValidationKit(tree_to_validate).binary_search_property()

    def test_single_node_is_true(self):
        validator = self._build_validator_with_treeroot(TreeNode(5))
        self.assertTrue(validator.validate_property())

    def test_manual_valid_tree_true(self):
        root_node = TreeNode(5)
        root_node.left_child = TreeNode(1)
        root_node.right_child = TreeNode(9)

        validator = self._build_validator_with_treeroot(root_node)
        self.assertTrue(validator.validate_property())

    def test_manual_complex_invalid_tree_false(self):
        root_node = TreeNode(8)
        root_node.left_child = TreeNode(4)
        root_node.left_child.left_child = TreeNode(2)
        root_node.left_child.right_child = TreeNode(12)

        root_node.right_child = TreeNode(10)
        root_node.right_child.right_child = TreeNode(20)

        validator = self._build_validator_with_treeroot(root_node)
        self.assertFalse(validator.validate_property())

    def test_manual_simple_invalid_tree_false(self):
        root_node = TreeNode(5)
        root_node.left_child = TreeNode(9)
        root_node.right_child = TreeNode(1)

        validator = self._build_validator_with_treeroot(root_node)
        self.assertFalse(validator.validate_property())

    def test_auto_tree_valid(self):
        tree = BinaryTreeKit().base_tree()
        tree.insert(TreeNode(50))
        tree.insert(TreeNode(50))
        tree.insert(TreeNode(51))
        tree.insert(TreeNode(55))
        tree.insert(TreeNode(99))
        tree.insert(TreeNode(30))

        validator = self._build_validator_with_treeroot(tree.root)
        self.assertTrue(validator.validate_property())
