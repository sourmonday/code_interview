
import unittest

from random import randint

from hard import *


class TestAddWithoutPlus(unittest.TestCase):
    def test_get_right_bit(self):
        self.assertEqual(1, get_nth_right_bit(1, 1))
        self.assertEqual(0, get_nth_right_bit(1, 2))

        self.assertEqual(0, get_nth_right_bit(4, 1))
        self.assertEqual(0, get_nth_right_bit(4, 2))
        self.assertEqual(1, get_nth_right_bit(4, 3))

    def test_set_right_bit(self):
        self.assertEqual(1, set_nth_right_bit(0, 1))
        self.assertEqual(5, set_nth_right_bit(4, 1))


    def test_works_random_adds_obj(self):
        for _ in range(100):
            num1 = randint(0, 10000)
            num2 = randint(0, 100)

            self.assertEqual(AddWithoutPlus(num1, num2).add(), num1 + num2)
