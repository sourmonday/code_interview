#!/usr/bin/python3

from data_structs.binary_trees import BaseBinaryTree
from data_structs.nodes import TreeNode
from data_structs.stack_queue import Queue, Stack


class NodeQueue(Queue):
    def enqueue(self, item):
        assert isinstance(item, TreeNode)
        super().enqueue(item)

    def dequeue(self) -> TreeNode:
        return super().dequeue()

    def peek(self) -> TreeNode:
        return super().peek()


class NodeStack(Stack):
    def pop(self) -> TreeNode:
        return super().pop()

    def peek(self) -> TreeNode:
        return super().peek()


class HardBiNode:
    def __init__(self, tree: BaseBinaryTree):
        self.tree = tree
        self.processing_queue = NodeQueue()
        self.processing_stack = NodeStack()
        self.list_head = None
        self.run_complete = False

    def convert_to_double_linked_list(self) -> 'HardBiNode':
        if self.tree.root:
            # self.in_order_traversal(self.tree.root)
            self.in_order_traversal_iterative()
            self.list_head = self.processing_queue.peek()
            self.fix_node_pointers_from_queue()
        self.run_complete = True
        return self

    def in_order_traversal(self, current_node: TreeNode):
        if current_node.left_child:
            self.in_order_traversal(current_node.left_child)

        self.processing_queue.enqueue(current_node)
        if current_node.right_child:
            self.in_order_traversal(current_node.right_child)

    def in_order_traversal_iterative(self):
        current = self.tree.root

        while current or self.processing_stack:
            while current:
                self.processing_stack.push(current)
                current = current.left_child

            current = self.processing_stack.pop()
            self.processing_queue.enqueue(current)
            current = current.right_child


    def fix_node_pointers_from_queue(self):
        current = self.processing_queue.dequeue()
        while self.processing_queue:
            current.set_right_child(self.processing_queue.peek())
            self.processing_queue.peek().set_left_child(current)
            current = self.processing_queue.dequeue()

    def order_repr(self) -> list:
        repr_queue = Queue()
        if self.run_complete:
            current_node = self.list_head
            while current_node:
                repr_queue.enqueue(current_node.value)
                current_node = current_node.right_child
            return list(repr_queue)
        return list()


if __name__ == '__main__':
    pass