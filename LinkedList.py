
class Node:
    def __init__(self, value):
        self.value = value
        self.next = None
        self.prev = None

    def __repr__(self):
        return str(self.value)

    def __eq__(self, other):
        return self.value is other.value


class SinglyLinkedList:
    def __init__(self):
        self.head = None
        self.size = 0

    def insert(self, item):
        if isinstance(item, Node):
            self._insert_node(item)
        else:
            self._insert_value(item)

    def _insert_value(self, value):
        self._insert_node(Node(value))

    def _insert_node(self, node):
        assert(isinstance(node, Node))
        if not self.head:
            self.head = node
        else:
            last_node = self.head
            while last_node.next:
                last_node = last_node.next

            last_node.next = node

        self.size += 1

    def remove_next(self, node):
        removed_node = node.next

        node.next = node.next.next
        self.size -= 1
        return removed_node

    def remove_head(self):
        removed_node = self.head

        self.head = self.head.next
        self.size -= 1
        return removed_node

    def __len__(self):
        return self.size

    def __repr__(self):
        repr_list = list()
        current_node = self.head

        while current_node:
            repr_list.append(current_node)
            current_node = current_node.next

        return ' .. '.join(map(str, repr_list))


class LinkedListQueue(SinglyLinkedList):
    def enqueue(self, item):
        self.insert(item)

    def dequeue(self):
        return self.remove_head()


if __name__ == '__main__':
    queue = LinkedListQueue()

    queue.enqueue(1)
    queue.enqueue(2)
    queue.enqueue(3)
    queue.enqueue(4)
    print(queue.dequeue())

    print(queue)
