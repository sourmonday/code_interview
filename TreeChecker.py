
from collections import defaultdict

from LinkedList import SinglyLinkedList

from data_structs.binary_trees import BaseBinaryTree
from data_structs.nodes import TreeNode
from data_structs.stack_queue import Stack


class NodeDepth:
    def __init__(self, tree: BaseBinaryTree):
        self.tree = tree
        self.cached_state = None
        self.depth_lists = defaultdict(SinglyLinkedList)
        self.node_depths = dict.fromkeys(self.tree.nodes)

    def generate_depth_lists(self):
        if not self._tree_state_changed():
            return

        stack = Stack()
        current = self.tree.root
        self.node_depths[current] = 1
        stack.push(current)

        while stack:
            current = stack.pop()
            self.depth_lists[self._get_depth(current)].insert(current)

            self._set_depth(current.left_child, self._get_depth(current) + 1)
            self._set_depth(current.right_child, self._get_depth(current) + 1)

            self._push_if_safe(current.right_child, stack)
            self._push_if_safe(current.left_child, stack)

        self._mark_tree_state()

    def get_depth_list(self, depth):
        return self.depth_lists[depth]

    def _set_depth(self, node, depth):
        self.node_depths[node] = depth

    def _get_depth(self, node):
        try:
            return self.node_depths[node]
        except KeyError:
            return 0

    @staticmethod
    def _push_if_safe(node, stack):
        if node is not None:
            stack.push(node)

    def _mark_tree_state(self):
        self.cached_state = self._get_tree_state(self.tree)

    def _tree_state_changed(self):
        return self._get_tree_state(self.tree) != self.cached_state

    @staticmethod
    def _get_tree_state(tree):
        return repr(tree)


if __name__ == '__main__':
    t = BaseBinaryTree()

    t.insert(TreeNode(5))
    t.insert(TreeNode(2))
    t.insert(TreeNode(1))
    t.insert(TreeNode(6))

    print(t)

    nd = NodeDepth(t)
    nd.generate_depth_lists()

    print(nd.depth_lists)
