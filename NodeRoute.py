
from DiGraph import DirectedGraph, DirectedGraphNode
from data_structs.stack_queue import Queue


class DiGraphBFS:
    def __init__(self, graph: DirectedGraph):
        self.graph = graph
        self.processing_queue = Queue()
        self.visited = dict.fromkeys(self.graph.nodes, False)

    def visit_and_queue_neighbors(self, node: DirectedGraphNode):
        assert(node in self.graph.nodes)
        self.visited[node] = True

        for neighbor in node.adj_list:
            if not self.visited[neighbor] and node not in self.processing_queue:
                self.processing_queue.enqueue(neighbor)

    def bfs(self, source: DirectedGraphNode, destination: DirectedGraphNode):
        if source not in self.graph.nodes or destination not in self.graph.nodes:
            return False

        self.processing_queue.enqueue(source)

        while self.processing_queue:
            visited_node = self.processing_queue.dequeue()
            self.visit_and_queue_neighbors(visited_node)

            if visited_node is destination:
                return True

        return False


if __name__ == '__main__':
    g = DirectedGraph()

    g.add_new_node_valued_as(1)
    g.add_new_node_valued_as(2)
    g.add_new_node_valued_as(3)
    g.add_new_node_valued_as(4)
    g.add_new_node_valued_as(5)
    g.add_new_node_valued_as(6)
    g.add_new_node_valued_as(7)
    g.add_new_node_valued_as(8)

    g.add_edge(g.get_node_with_value(1), g.get_node_with_value(2))
    g.add_edge(g.get_node_with_value(1), g.get_node_with_value(3))

    g.add_edge(g.get_node_with_value(2), g.get_node_with_value(4))
    g.add_edge(g.get_node_with_value(2), g.get_node_with_value(5))

    g.add_edge(g.get_node_with_value(3), g.get_node_with_value(6))

    g.add_edge(g.get_node_with_value(4), g.get_node_with_value(7))

    g.add_edge(g.get_node_with_value(5), g.get_node_with_value(7))

    g.add_edge(g.get_node_with_value(6), g.get_node_with_value(5))
    g.add_edge(g.get_node_with_value(6), g.get_node_with_value(7))

    print(g)

    bfs = DiGraphBFS(g)
    print(bfs.bfs(g.get_node_with_value(1), g.get_node_with_value(8)))
