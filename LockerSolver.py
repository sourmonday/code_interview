
"""
100 Lockers: There are 100 closed lockers in a hallway. A man begins by opening all 100
lockers. Next, he closes every second locker. Then on his third pass, he toggles
every third, etc. After his 100th pass, how many lockers are open?
"""

from collections import defaultdict


class LockerSolver:
    def __init__(self):
        pass

    @staticmethod
    def num_open(count):
        lockers = LockerSolver._run_collision_gen(count)
        toggled = [len(v) % 2 for _, v in lockers.items()]
        return toggled.count(1)

    @staticmethod
    def _run_collision_gen(count):
        lockers = defaultdict(set)

        for x in range(1, count + 1):
            expanded = set(filter(lambda multiples: multiples % x == 0, range(1, count + 1)))
            for index in expanded:
                lockers[index].add(x)
        return lockers


if __name__ == '__main__':
    num_open_lockers = LockerSolver().num_open(100)
    print(num_open_lockers)
