
"""
Copied from daily_coding.md

# Problem 19

Given a N x K matrix where the nth row and kth column represents the cost to build the nth house with the kth color,
return the minimum cost which achieves this goal.

Imagine that you only have to build a row of 1 house. This one is obvious, you will build the house for the cost
cost_matrix[0][0]. You have to start here.

If you have to build a row of 2 houses. You have to build the houses for the cost cost_matrix[0][0] + cost_matrix[0][1]

Once you hit 3, however, that's where choices differ. Do you build with a third color or do you build the 2nd house with
one of the already used colors (no qualitative difference of colors)? And so on and so forth.

It might be that after passing some arbitrary point in nth house with kth color, the cost of reusing a color just gets
that much cheaper. If you don't include the constraint of no consecutive colors, this problem lends itself to a shortest
path graph algorithm, where the path cost represents the cost of iterating on the next building.

With the color constraint, you have to keep track of the N-1 color and ensure that you do not look that up.

Terminate upon reaching the first path that terminates at N houses and that is your answer.


"""
import random

def generate_cost_matrix(houses, colors):
    cost_matrix = list()
    for n in range(houses):
        color_cost = list()
        for color in range(colors):
            color_cost.append(random.randint(100000, 300000))
        cost_matrix.append(color_cost)
    return cost_matrix


if __name__ == '__main__':
    print(generate_cost_matrix(4, 5))
