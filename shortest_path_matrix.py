
"""
# Problem 23

Given an M by N matrix, a start coordinate, and an end coordinate return the minimum number of steps to reach the end
coordinate from the start. If no such path exists, return NULL.

The matrix consists of booleans, True is a wall, False is passable terrain

This is a shortest-path problem. Conceptualizing each coordinate as a node in a graph, you do a breadth-first-search
to look for the end coordinate. Prevent loops by using a hash (Python set) of visited coordinates.

For each coordinate, look around to see which vertically or horizontally adjacent coordinates you can step onto

"""


from math import inf


# Board is indexed into with Board[x][y], Board[x] gives you the column

class Coordinate:
    def __init__(self, x, y):
        if x > 9999 or y > 9999:
            raise NotImplementedError
        self.x = x
        self.y = y

    def is_valid(self, max_dimensions: 'Coordinate'):
        return self.x >= 0 and self.y >= 0 and self.x < max_dimensions.x and self.y < max_dimensions.y

    def __repr__(self):
        return "(%d, %d)" % (self.x, self.y)

    def __eq__(self, other: 'Coordinate'):
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash(self.x + self.y * 10000)  # magic number here

class Board:
    def __init__(self, max_x: int, max_y: int):
        self.dimensions = Coordinate(max_x, max_y)
        self.board = self.__initialize_board(max_x, max_y)

    @staticmethod
    def __initialize_board(max_x, max_y):
        board = list()
        for x in range(max_x):
            column = [False] * max_y
            board.append(column)
        return board


class PathSolver:
    def __init__(self, start: Coordinate, end: Coordinate, board: Board):
        self.start = start
        self.end = end
        self.board = board
        self.distances = self.__init_distance_board(board.dimensions.x, board.dimensions.y)
        self.distances.board[start.x][start.y] = 0

    def solve(self):
        queue = list()
        queue.append(self.start)
        visited = {self.start}

        while queue:
            current_node = queue.pop(0)
            neighbors = self.get_possible_neighbors(current_node, self.board)

            for n in neighbors:
                assert isinstance(n, Coordinate)
                if n == self.end:
                    return self.distances.board[current_node.x][current_node.y] + 1
                if n not in visited:
                    queue.append(n)
                    visited.add(n)
                    self.distances.board[n.x][n.y] = self.distances.board[current_node.x][current_node.y] + 1

        return None

    @staticmethod
    def __init_distance_board(max_x: int, max_y: int):
        board = Board(max_x, max_y)
        for x_index in range(max_x):
            board.board[x_index] = [inf] * max_y
        return board

    @staticmethod
    def get_possible_neighbors(coordinate: Coordinate, board: Board) -> tuple:
        up = Coordinate(coordinate.x, coordinate.y - 1)
        down = Coordinate(coordinate.x, coordinate.y + 1)
        left = Coordinate(coordinate.x - 1, coordinate.y)
        right = Coordinate(coordinate.x + 1, coordinate.y)

        valid_neighbors = tuple(filter(lambda x: x.is_valid(board.dimensions), (up, down, left, right)))
        return tuple(filter(lambda coord: not board.board[coord.x][coord.y], valid_neighbors))


if __name__ == '__main__':
    b = Board(4, 4)
    b.board[1] = [True, True, False, True]
    start = Coordinate(3, 0)
    end = Coordinate(0, 0)

    solver = PathSolver(start, end, b)
    print(solver.solve())
