
from data_structs.stack_queue import Stack

class MinSortedStack(Stack):
    """
    Stack needs to support push, pop, peek, and isEmpty

    isEmpty is implemented by superclass with __len__()
    peek & pop is implemented at superclass

    """
    def __init__(self):
        super().__init__()
        self.buffer_stack = Stack()

    def push(self, item):
        if self._is_okay_to_push(item):
            super().push(item)
        else:
            while self and item > self.peek():
                self.buffer_stack.push(self.pop())

            super().push(item)
            while self.buffer_stack:
                super().push(self.buffer_stack.pop())

    def _is_okay_to_push(self, item):
        try:
            item <= self.peek()
        except IndexError:
            return True


if __name__ == '__main__':
    sorted_stack = MinSortedStack()

    sorted_stack.push(1)
    sorted_stack.push(6)
    sorted_stack.push(3)
    sorted_stack.push(1)
    sorted_stack.push(2)
    sorted_stack.push(1)

    print(sorted_stack)
