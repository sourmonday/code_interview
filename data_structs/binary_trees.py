
from data_structs.exceptions import AVLRebalancingError
from data_structs.nodes import *
from data_structs.stack_queue import Stack


class BinaryTreeKit:
    def __init__(self):
        pass

    @staticmethod
    def base_tree():
        return BaseBinaryTree()

    @staticmethod
    def avl_tree():
        return AVLTree()

    def red_black_tree(self):
        raise NotImplementedError


class BaseBinaryTree:
    def __init__(self):
        self.root = None
        self.size = 0
        self.nodes = list()

    def set_root(self, node: TreeNode):
        self.root = node
        self.size = 1
        self.nodes = list([self.root])

        processing_stack = Stack([self.root.get_right_child(), self.root.get_left_child()])

        while processing_stack:
            current = processing_stack.pop()
            if not current:
                continue

            assert isinstance(current, TreeNode)
            processing_stack.push(current.get_right_child())
            processing_stack.push(current.get_left_child())

            self._update_tree_nodes_and_size(current)

    def insert(self, node: TreeNode) -> Stack:
        self._update_tree_nodes_and_size(node)

        if not self._insert_at_root_if_available(node):
            return self._insert_at_nonroot_position(node)

        return Stack([self.root])

    def _update_tree_nodes_and_size(self, node: TreeNode):
        self.nodes.append(node)
        self.size += 1

    def _insert_at_root_if_available(self, node: TreeNode) -> Stack:
        if not self.root:
            self.root = node
            return Stack([self.root])

    def _insert_at_nonroot_position(self, node: TreeNode) -> Stack:
        assert self.root
        ancestry_stack = Stack()

        current_node = self.root

        while current_node:
            ancestry_stack.push(current_node)
            current_node = self._next_iter_insertion_traversal(node, current_node)

        return ancestry_stack

    @staticmethod
    def _next_iter_insertion_traversal(insert_node, other):
        if insert_node <= other:
            if other.left_child is None:
                other.left_child = insert_node
                return None
            else:
                return other.left_child
        else:
            if other.right_child is None:
                other.right_child = insert_node
                return None
            else:
                return other.right_child

    def __len__(self):
        return self.size

    def __repr__(self):
        return str.format('tree of size: {}', self.size) + str(self.nodes)


class AVLTree(BaseBinaryTree):
    def insert(self, node: TreeNode):
        ancestry_stack = super().insert(node)

        if self._stack_more_than_just_root(ancestry_stack):
            with ancestry_stack as still_have_nodes_to_fix_balance_factors_for:
                while still_have_nodes_to_fix_balance_factors_for:
                    backtrack_node = ancestry_stack.pop()
                    assert isinstance(backtrack_node, AVLNode)

                    backtrack_node.update_avl_balance_and_height()

                    if backtrack_node.is_unbalanced():
                        if backtrack_node.get_balance_factor() is BalanceFactor.left_heavy:
                            self._rebalance_left_heavy(backtrack_node, ancestry_stack.peek())
                        elif backtrack_node.get_balance_factor() is BalanceFactor.right_heavy:
                            self._rebalance_right_heavy(backtrack_node, ancestry_stack.peek())
                        else:
                            raise AVLRebalancingError

    @staticmethod
    def _stack_more_than_just_root(stack: Stack):
        return len(stack) > 1

    def _rebalance_left_heavy(self, current_node: AVLNode, node_parent: AVLNode):
        left_child = current_node.get_left_child()

        if left_child.get_balance_factor() is BalanceFactor.left_balanced:
            new_pivot_node = self._right_rotation(current_node)
        else:
            new_pivot_node = self._left_right_rotation(current_node)

        if node_parent:
            node_parent.set_left_child(new_pivot_node)
        else:
            self.root = new_pivot_node

    def _rebalance_right_heavy(self, current_node: AVLNode, node_parent: AVLNode):
        right_child = current_node.get_right_child()

        if right_child.get_balance_factor() is BalanceFactor.right_balanced:
            new_pivot_node = self._left_rotation(current_node)
        else:
            new_pivot_node = self._right_left_rotation(current_node)

        if node_parent:
            node_parent.set_right_child(new_pivot_node)
        else:
            self.root = new_pivot_node

    @staticmethod
    def _right_left_rotation(node: AVLNode) -> AVLNode:
        AVLTree._right_subtree_right_rotation(node)
        return AVLTree._left_rotation(node)

    @staticmethod
    def _left_right_rotation(node: AVLNode) -> AVLNode:
        node = AVLTree._left_subtree_left_rotation(node)
        return AVLTree._right_rotation(node)

    @staticmethod
    def _right_subtree_right_rotation(node: AVLNode) -> AVLNode:
        right_child = node.get_right_child()
        rl_grandchild = right_child.get_left_child()

        right_child.set_left_child(rl_grandchild.get_right_child())
        rl_grandchild.set_right_child(right_child)
        node.set_right_child(rl_grandchild)

        right_child.update_avl_balance_and_height()
        rl_grandchild.update_avl_balance_and_height()
        node.update_avl_balance_and_height()

        return node

    @staticmethod
    def _left_subtree_left_rotation(node: AVLNode) -> AVLNode:
        left_child = node.get_left_child()
        lr_grandchild = left_child.get_right_child()

        left_child.set_right_child(lr_grandchild.get_left_child())
        lr_grandchild.set_left_child(left_child)
        node.set_left_child(lr_grandchild)

        left_child.update_avl_balance_and_height()
        lr_grandchild.update_avl_balance_and_height()
        node.update_avl_balance_and_height()

        return node

    @staticmethod
    def _right_rotation(node: AVLNode) -> AVLNode:
        left_child = node.get_left_child()
        left_grandchild = left_child.get_left_child()

        node.set_left_child(left_child.get_right_child())

        left_child.set_right_child(node)

        node.update_avl_balance_and_height()
        left_grandchild.update_avl_balance_and_height()
        left_child.update_avl_balance_and_height()

        return left_child

    @staticmethod
    def _left_rotation(node: AVLNode) -> AVLNode:
        right_child = node.get_right_child()
        right_grandchild = right_child.get_right_child()

        node.set_right_child(right_child.get_left_child())

        right_child.set_left_child(node)

        node.update_avl_balance_and_height()
        right_grandchild.update_avl_balance_and_height()
        right_child.update_avl_balance_and_height()

        return right_child


if __name__ == '__main__':
    t = BinaryTreeKit().avl_tree()

    t.insert(AVLNode(5))
    t.insert(AVLNode(1))
    t.insert(AVLNode(3))

    print(t)
