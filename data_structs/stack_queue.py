
from data_structs.constants import EMPTY_STACK


class Stack:
    def __init__(self, iterable=EMPTY_STACK):
        self.stack = list(iterable)

    def push(self, item):
        self.stack.append(item)

    def pop(self):
        try:
            return self.stack.pop()
        except IndexError:
            return None

    def peek(self):
        try:
            return self.stack[-1]
        except IndexError:
            return None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def __len__(self):
        return self.stack.__len__()

    def __repr__(self):
        return str(self.stack)


class Queue(list):
    def enqueue(self, item):
        self.append(item)

    def dequeue(self):
        return self.pop(0)

    def peek(self):
        return self[0]


class MyQueue:
    def __init__(self):
        self.fifo_stack = Stack()
        self.lifo_stack = Stack()

    def enqueue(self, item):
        self.lifo_stack.push(item)

    def dequeue(self):
        if not self.fifo_stack:
            while self.lifo_stack:
                self.fifo_stack.push(self.lifo_stack.pop())

        return self.fifo_stack.pop()

    def __repr__(self):
        return '..'.join(map(str, (self.fifo_stack, self.lifo_stack)))


if __name__ == '__main__':
    queue = MyQueue()
    queue.enqueue(1)
    queue.enqueue(2)
    queue.enqueue(3)
    queue.enqueue(4)
    print(queue.dequeue())

    print(queue)
