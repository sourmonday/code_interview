
from data_structs.constants import BalanceFactor, NONE_HEIGHT


class BinaryNodeKit:
    def __init__(self):
        pass

    @staticmethod
    def base_node(value):
        return TreeNode(value)

    @staticmethod
    def avl_node(value):
        return AVLNode(value)

    @staticmethod
    def red_black_node(value):
        raise NotImplementedError


class TreeNode:
    def __init__(self, value):
        self.value = value
        self.left_child = None
        self.right_child = None

    def set_left_child(self, other: 'TreeNode'):
        self.left_child = other

    def set_right_child(self, other: 'TreeNode'):
        self.right_child = other

    def get_left_child(self) -> 'TreeNode':
        return self.left_child

    def get_right_child(self) -> 'TreeNode':
        return self.right_child

    def is_leaf(self) -> bool:
        return not self.has_right_child() and not self.has_right_child()

    def has_right_child(self) -> bool:
        return self.right_child is not None

    def has_left_child(self) -> bool:
        return self.left_child is not None

    def __lt__(self, other):
        return self.value < other.value

    def __le__(self, other):
        return self.value <= other.value

    def __gt__(self, other):
        return self.value > other.value

    def __ge__(self, other):
        return self.value >= other.value


class AVLNode(TreeNode):
    def __init__(self, value):
        super().__init__(value)
        self._balance_factor = BalanceFactor.balanced
        self._node_height = 1

    def set_left_child(self, other: 'AVLNode'):
        super().set_left_child(other)
        self.update_node_height()
        self.update_balance_factor()

    def set_right_child(self, other: 'AVLNode'):
        super().set_right_child(other)
        self.update_node_height()
        self.update_balance_factor()

    def update_balance_factor(self):
        left_height = self._height_getter_helper(self.left_child)
        right_height = self._height_getter_helper(self.right_child)

        self._balance_factor = BalanceFactor(left_height - right_height)

    def update_node_height(self):
        left_height = self._height_getter_helper(self.left_child)
        right_height = self._height_getter_helper(self.right_child)

        self._node_height = max(left_height, right_height) + 1

    def update_avl_balance_and_height(self):
        self.update_node_height()
        self.update_balance_factor()

    @staticmethod
    def _height_getter_helper(other : 'AVLNode'):
        # TODO: figure out another way besides using this way to deal with NoneType issues...
        if other:
            return other.get_node_height()
        return NONE_HEIGHT

    def get_node_height(self):
        return self._node_height

    def get_balance_factor(self):
        return self._balance_factor

    def is_unbalanced(self):
        return self._balance_factor is BalanceFactor.right_heavy or self._balance_factor is BalanceFactor.left_heavy

