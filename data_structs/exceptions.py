
class AVLRebalancingError(Exception):
    pass


class BalancedTreeTypeNotImplemented(NotImplementedError):
    pass
