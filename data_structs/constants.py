
from enum import Enum

NONE_HEIGHT = 0
EMPTY_STACK = ()


class BalanceFactor(Enum):
    right_heavy = -2
    right_balanced = -1
    balanced = 0
    left_balanced = 1
    left_heavy = 2
