These are notes. As long as I know what to code, I'm moving on




# Problem 1

List of numbers and some number k, return true if any two numbers in the list sum up to k.

Bonus: do it in one pass

### Answer

As you iterate through, calculate the other number that you need to see in order to return true. Question, do we know
if the list will have negative numbers?

As you calculate the other number, add the difference to a HashMap (set) ```true_if_seen``` and check if the number
you are processing collides with a value in that HashMap. If so, terminate and return true.

This is a one pass solution.

---

# Problem 2

Given an array of integers, return a new array such that each element at index i of the new array is the product of all
numbers in the original array except the one at i

Example:
[1, 2, 3, 4, 5] gives [120, 60, 40, 30, 24]
[3, 2, 1] gives [2, 3, 6]

### Answer

Obvious solution is to find the ```grand_product``` in one pass and then divide results for the new array on a second
pass to make your new array. This is a two pass solution, O(n) If returning in-place, then O(1), otherwise O(n).

Follow-up is what you do if you cannot use division

Brute-force is to do something like:

```python
from collections import Iterable

def product(it: Iterable) -> int:
    grand_product = 1
    for item in it:
        grand_product *= item
    return grand_product


l = list()
new_list = list()
for index,_ in enumerate(l):
    elem_product = product(l[:index]+l[index+1:])  # this might have an off by one
    new_list.append(elem_product)

```

Double for loop gives this a O(n^2) runtime.

If you imagine each element at index, i, as a set bit in a bitstream, you could use that as a mechanism to know which
products correspond to each elements being in there.

On a single pass, if I multiply everything together:

[1, 2, 3, 4, 5]

[10000: 1, 11000: 2, 11100: 6, 11110: 24, 11111: 120]
The last value is useless, however, the values at this ordered_dict[0:3] are useful. Element[11100]-> 6 Can be reused
to calculate the Element[11101], which would be placed at element 3 in the original array location.

If you take a backwards pass:

[11111: 120, 01111: 120, 00111: 60, 00011: 20, 00001: 5]

Each of the values in the two defined ordered_dicts have a corresponding element that gives you your bitstream with only
one bit switched off. 

Three-pass solution, O(n) with O(n) space requirements.

Removing the useless values and shift right
            [10000: 1,  11000: 2,  11100: 6, 11110: 24] // forward
[01111: 120, 00111: 60, 00011: 20, 00001: 5]            // backward

```python
class MultiplyOthers:
    def __init__(self, question: list):
        self.grand_product = list()
        self.question_list = question
        self.__calculate()
        
    @staticmethod
    def __build_products(patient_list):
        ret_list = [patient_list[0]]
        for index in range(1, len(patient_list) - 1):  # ends before the last element
            ret_list.append(ret_list[-1] * patient_list[index])
        return ret_list
        
    def __calculate(self):
        forward_products = self.__build_products(self.question_list)
        backwards_products = self.__build_products(self.question_list[::-1])
        
        for index, forward_element in enumerate(forward_products):
            if index == len(forward_products) - 1:
                break
            with backwards_products[index + 1] as backwards_element:
                self.grand_product.append(forward_element*backwards_element)
        self.grand_product.append(forward_products[-1])
        self.grand_product.insert(0, backwards_products[0])
        
    def get_answer(self):
        return self.grand_product
        
```

---

# Problem 3

Given the root to a binary tree, implement serialize(root), which serializes the tree into a string, and deserialize(s),
which deserializes the string back into the tree

Given:
```python
def serialize(root):
    pass
    
def deserialize(s):
    pass


class Node:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

# The following test should pass

node = Node('root', Node('left', Node('left.left')), Node('right'))
assert deserialize(serialize(node)).left.left.val == 'left.left'
```

### Answer
Instead of coming up with a full schema, you can take the approach of two conversions and use the indexing you see in
list representations of heaps.

You can then separate each element of that list with new-lines. You don't have to encode left-right because that is 
inherent in the structure. You know to go to the left child of root, which is at line 1, you go to line 2. Line 3 for
the right child. You put the value on the line. Question to the interviewer, are all values going to be strings?

Assuming that it is:

convert_tree_to_list
['root', 'left', 'right', 'left.left', None, None, None]

Then convert list_to_string:
root\nleft\nright\nleft.left\n\n\n\n

convert_string_to_list
read_lines(string)

convert_list_to_tree:
```python
# TODO
```

--- 

# Problem 4

Given array of integers, find the first missing positive integer in linear time and constant space. In other words, find
the lowest positive integer that does not exist in the array. Duplicates and negatives exist in the array.

### Answer

There are three possible states for the answer. One is that the answer is 1, the lowest possible positive integer. This
is possible when 1 does not exist in the array.

The second possible state is the array_max + 1, this is possible when the array covers all numbers in the range 1 to
array_max. You can only answer with array_max + 1 when you know that all numbers are in the array.

The last possible state is some arbitrary lowest integer in between 1 and array_max where there is a hole in the 
coverage.

First state check is easy.  ```first_state = 1 in array_stream```

---

# Problem 7

Given a mapping a to z correspondings to the integers 1 to 26, and an encoded message stream of integers, count the 
number of ways that the encoded stream could be interpreted.

### Answer

You know that all numbers from 3-9 are independent. They must represent some number. However, the number before it may 
with it in order to make one number or two if the number preceding is a 1 or a 2.

You can make an O(n) first pass to count the numbers of 3-9 that are not preceded by a 1 or a 2. During this pass, you
can also look for any 0s as that must follow a 1 or a 2 and takes away the ability for that 1 or 2 to represent two or
more possibilities.

But if you do it this way, you have to maintain knowledge of spacing for the digits that you remove.

With the insight that 0s mean nothing besides a 10 or a 20, you can do a first pass without expansion for all single
numbers by taking the length of the stream - the number of 0s detected.

Now you only need to count numbers from expansion. You know these number streams must start with a 1 or a 2. If the next
number is a 1 or a 2 again, you add one for each 1 or 2 that is non-adjacent to a 0.

2*(# of 1s and 2s) - (# of 0s) + (# of 3-9)

---

# Problem 9

Given a list of integers, write a function that returns the largest sum of non-adjacent numbers.
Numbers can be 0 or negative.

### Answer

This is similar to the masseuse scheduling problem. Due to adjacency constraints, you have a general rule of three
consecutive numbers, to start. However, in this case, negative numbers make it so you cannot just simply apply that 
rule

Thee rule is that it doesn't make sense for three consecutive appointments  to be skipped. This is cleearly broken by
negative numbers. Zeroes don't affect the sum. 

Case of two:
{x, y}, select the larger of the two

Case of three
{x, y, z}, select y iff y > x + z
x, z

Case of four
{w, x, y, z} max( {w,y}, {x, z}, {w, z}, w, x, y, z)

Case of five
{v, w, x, y, z} max( {v, x, z}, {w, y} {x, z}, v, w, x, y, z)

Recursion

# Problem 28

Algorithm to justify text, given list

Define a method can_add(line, limit, word). Given the line and the char limit of that line, and that the line is not
empty, check if the line can add the next word by seeing if len(line) + len(stack.peek) + 1 > 16

(I know it's not a stack, just using it for brevity)

Instead of calculating the length of the line every time, you store each line as a list of two elements 
[LINE_LEN, [LIST_OF_WORDS]]. You can also wait to process the spaces. LINE_LEN will not equal the sum total of the
lengths of the words in LIST_OF_WORDS for this reason.

Once all of your words have been assigned to a particular line, go back to your intermediate data-structure and:

```python
document_lines = list()
limit = 16
for line_index, line in enumerate(document_lines):
    line_len, list_of_words = tuple(line)
    additional_spaces = limit - line_len
    fully_filled_amount = additional_spaces // len(list_of_words)
    additional_spaces_per_word = [fully_filled_amount] * (len(list_of_words) - 1)
    
    for index in range(additional_spaces % len(list_of_words)):
        additional_spaces_per_word[index] += 1
    
    for index, spaces_number in enumerate(additional_spaces_per_word):
        list_of_words[index] += " " * (spaces_number + 1)
        
    document_lines[line_index] = ''.join(list_of_words)
```

For production code, you might assert that the first word you enter is always less than the length of the limit and
also assert that the justified lines meet your intent.

---

# Problem 29

Given a string with repeated successive characters, create the run-length encoding.

No constraints are given. If you interpret the string as an indexed character array, you can run forward from each
start of the new point until you find a non-duplicate character. That's your count. Skip forward on your loop to start
from a new head.

AAAABBBCC encoded is 4A + encode(BBBCC)

In production, you might look at performance of string concatenation or stack space requirements etc to see which
solution should be committed.

```python
def encoding(string):
    index = 0
    final_string = list()
    while index < len(string):
        current_char = string[index]
        char_counter = 0
        while current_char == string[index] and index < len(string):
            char_counter += 1
            index += 1
        final_string.append(str(char_counter) + current_char)
    return ''.join(final_string)

```

This is O(n) time and O(n) space

---

# Problem 27

Given a string with potentially round, curly, or square braces/brackets, return whether or not the brackets are balanced

Properties 

In order for a string of braces/brackets to be considered balanced, any closing brace must have an open opening brace
that has not yet been closed AND matches in type.
You cannot close more than you open.
You must close exactly as many times as you open.

This type of problem is an exact match for Stacks.

```python
class Stack:
    def push(self, item):
        pass
     
    def pop(self):
        assert len(self) > 0
        pass
        
    def peek(self):
        pass
        
    def __len__(self):
        pass

def is_open_brace(brace_char):
    if brace_char == '[' or brace_char == '(' or brace_char == '{':
        return True
    return False
    
def is_brace_type(brace_char):
    if brace_char == '[' or brace_char == '(' or brace_char == '{' or \
        brace_char == ')' or brace_char == '}' or brace_char == ']':
        return True
    return False
    
# Not Implemented
def brace_matches(open, close):
    return False
    

def solve_braces(string):
    s = Stack()
    try:
        for brace in string:
            assert is_brace_type(brace)
            if is_open_brace(brace):
                s.push(brace)
            else:
                assert brace_matches(s.pop(), brace)
        assert len(s) is 0
    except AssertionError:
        return False
        
    return True    
```

---

# Problem 23

Given an M by N matrix, a start coordinate, and an end coordinate return the minimum number of steps to reach the end
coordinate from the start. If no such path exists, return NULL. 

The matrix consists of booleans, True is a wall, False is passable terrain

This is a shortest-path problem. Conceptualizing each coordinate as a node in a graph, you do a breadth-first-search
to look for the end coordinate. Prevent loops by using a hash (Python set) of visited coordinates.  

For each coordinate, look around to see which vertically or horizontally adjacent coordinoates you can step onto

---

# Problem 16

Given the need to record the last N order ids, implement a structure that implements
* record(order_id): adds order id to the log
* get_last(i): gets the i-th last element from the log, i is guaranteed to be smaller than N

Conceptually, use a doubly-linked circular linked list to create a rotating log of N elements. In practice, use an 
array (list) in order to achieve O(1) access time on get_last

```python

class OrderLog:
    def __init__(self, N: int):
        self.records = [None] * N
        self.max_records = N
        self.current_index = 0
        
    def record(self, order_id: int):
        self.current_index = int((self.current_index + 1) % self.max_records)
        self.records[self.current_index] = order_id

    def get_last(self, from_last: int):
        return self.records[self.current_index - from_last]
```

---

# Problem 8

A unival tree (which stands for "universal value") is a tree where all nodes under it have the same value.

Given the root to a binary tree, count the number of unival subtrees.


In my words: a unival tree is a tree where all nodes from the root of value x are also of value x.

All leaf nodes are a unival tree, by definition. An arbitrary, non-leaf node is a unival tree if its two children are
unival trees. Since the unival status of any node's children must be known prior to figuring out if that node could be
a unival tree, a post-order traversal is the natural solution for this problem.

If I can modify the data structure, I would add a is_unival boolean field. However, I'll assume I can't and use a 
dictionary.

```python
class Node:
    def __init__(self, value, left: 'Node', right: 'Node'):
        self.value = value
        self.left = left
        self.right = right
        
class Univals:
    def __init__(self, root: Node):
        self.unival_tree_count = 0
        self.root = root
        self.unival_nodes = dict()
        
    def count(self):
        self.__is_unival(self.root)
        return self.unival_tree_count
        
    def __is_unival(self, node: Node) -> bool:
        if not node.left and not node.right:
            self.unival_tree_count += 1
            return True
        if not node.left:
            if node.value != node.right.value:
                return False
            return self.__is_unival(node.right)
        if not node.right:
            if node.value != node.left.value:
                return False
            return self.__is_unival(node.left)
        
        value_matches = node.value == node.right.value and node.value == node.left.value
        left_is_unival = self.__is_unival(node.left)
        right_is_unival = self.__is_unival(node.right)
        if left_is_unival and right_is_unival and value_matches:
            self.unival_tree_count += 1
            return True
        return False    
            
```

---

# Problem 12

There exists a staircase with N steps and you can climb u p either 1 or 2 steps at a time. Given N, write a function
that returns the number of unique ways you can climb the staircase. The order of the steps matters.

For example, if N is 4, then there are 5 unique ways:

1,1,1,1
2,1,1
1,2,1
1,1,2
2,2

unique_ways(n) = unique_ways(n-1) + unique_ways(n-2)

Build from the base case and you realize that all new cases are essentially start with two steps then do the same
sequence of steps as n-2 or one step and then do the same sequnce as n-1.


What if, instead of being able to climb 1 or 2 steps at a time, you could climb any number from a set of positive
integers X? For example, if X = {1,3,5}, you could climb 1, 3, or 5 steps at a time.

unique_ways(n) = sum(unique_ways(element_X))

--- 

# Problem 13

Given an integer k and a string s, find the length of the longest substring that contains at most k distinct characters.

Given k and the at most k distinct characters, you know the worst case substring length is k. That is, the worse case
scenario of a sub-string of all distinct characters has a length of k.

Brute-forcing, how do you know a particular sub-string has an arbitrary k amount of distinct chars?

```python
substring = ""
k = 2

assert(len(set(substring)) == k)
```

Building the set from first-principles (perhaps there might be a better way), you need to go through k chars to build
the HashMap (set) and then O(1) access to the size of the set as long as you were counting set.adds on the way.

You would have to do this for all sub-strings, which is O(n! * n) = O(n!) or something else way too large

Distinct between two sub-strings is a union operator. set.union(other_set) is a potentially linear time operation,
depending on implementation (Hash Table based). A sufficiently great space will be required.

If you cache the results of specific substring runs and create a way to do look-ups to know that 3-5 is 3-4 + 5, for
example, in constant time, then:

You need to run through the 1s at least once, O(n)
The twos and beyond are derivations of the results already in the cache.

Overall, something approaching O(n^2) here?

You do get some additional time savings based on being able to drop something and all substrings that are required
to union against that thing if it exceeds the k amount of distinct chars. 

```python

```

---

# Problem 18

Given an array of integers and a number k, where 1 <= k <= length of the array, compute the maximum values of each
of length k.

For example, given array = [10, 5, 2, 7, 8, 7] and k = 3, we should get: [10, 7, 8, 8]

Do this in O(n) time and O(k) space

### Answer

First, ignore the constraints and get an accurate answer. For the sub-problem of computing any maximum of an arbitrary
sub-array, you want to look to the Binary Heap data structure with the maximum at the root. That gives you worst case
performance of O(log n) for insertion and deletion. To keep track of what elements need to be removed / replaced from
the MaxHeap sub-data-structure, you may use a queue to to keep track of the runs for O(1) insertion and deletion.

That gives you an O(2k) space requirement, which will resolve to O(k). Using this method, you need to do insertions
and deletions for n - k times. Your time complexity is O((n - k) log n). Assuming n scales harder than k, that becomees
O(n log n), in the worst case. If k is close to n, where the term (n - k) comes close to 1, you get close to O(log n)

Looking at the problem constraints, our space requirement is good, but our time is not.

```python
# TODO: Looking at the given solution, I don't quite believe that the solution meets the O(n) time standard. Review 
#  needed

```
---

# Problem 19

Given a N x K matrix where the nth row and kth column represents the cost to build the nth house with the kth color,
return the minimum cost which achieves this goal.

Imagine that you only have to build a row of 1 house. This one is obvious, you will build the house for the cost
cost_matrix[0][0]. You have to start here.

If you have to build a row of 2 houses. You have to build the houses for the cost cost_matrix[0][0] + cost_matrix[0][1]

Once you hit 3, however, that's where choices differ. Do you build with a third color or do you build the 2nd house with
one of the already used colors (no qualitative difference of colors)? And so on and so forth. 

It might be that after passing some arbitrary point in nth house with kth color, the cost of reusing a color just gets
that much cheaper. If you don't include the constraint of no consecutive colors, this problem lends itself to a shortest
path graph algorithm, where the path cost represents the cost of iterating on the next building. 

With the color constraint, you have to keep track of the N-1 color and ensure that you do not look that up.

Terminate upon reaching the first path that terminates at N houses and that is your answer.

---

# Problem 21

Given an array of time intervals (start, end) for classroom lectures, possibly overlapping, find the minimum number of
rooms required.

Brute-forcing it, you could go though O(n) intervals and split up each to labeled tuples 
(time_int, 'start or end label'), insert into a binary min heap as you go (log n insertion time). Then take out elements
and increment or decrement simultaneous classrooms variable until you are done with the heap. 

Deletion is also log n

First pass, O(n log n) with insertion into binary heap
Second pass, O(n log n)

Overall time O(n log n) with O(n) space

Best theoretical... you HAVE to go through at least O(n) to look at all time intervals

n log n is imposed by essentially sorting the times. Can you do it without sorting?

A human approach would be to color in horizontal bars and look for the time interval that has the great thickness. 

You could use a bitstream HashMap, where if you were to visualize it as an arrays of booleans: [False, False, True]
would represent that classrooms were unneeded for 2 minutes but used for the last. But keeping track of collision 
counts per section seems not worth it....

You could have an array of minutes and do an increment per minute. But then time would be O(m x n), where m is the 
maximum minute interval. You would only go through this method if m < log n. Assuming that m is a constant 
(60 x 24) = 1440, that's likely not going to happen.

---

# Problem 22

Given a dictionary of words and a string made up of those words without spaces, return the original sentence in a list.
If there is more than one possible reconstruction, return any of them. If no possible reconstruction, then return NULL.

As you run through that string, you can detect words in the dictionary if you convert that dictionary to a Trie. Once
a word is detected, however, that is where a decision must be made to either continue with the word to see if it is
part of a longer word or to start the next letter as part of a new word.

Some considerations, if the next letter in the string-stream is not a valid continuation, the next letter that follows
must be the start of a new word.

If the next letter is instead not a valid word, then it must be that the next character is a continuation to a longer
word.

Solve this using backtracking to the last valid path every time the second substring doesn't work. 

---

# Problem 25

Regular expression of:

* period matches any single character
* asterisk matches zero or more of the preceding element

If you only deal with the first portion of the regular expression, all you have to do is a one-for-one walk of both the
regular expression and the candidate string and return True.

For an asterisk, there are a few cases you have to deal with:

* (some letter)* : expand forward to eliminate all of that letter until you need to consider the next letter
* (some letter)*(some other letter) : same case as above
* (some letter)*(same letter)... you need to expand while having backtracking because the program won't know off the bat
which one you mean
* .* : this can expand to match any arbitrary numbers of letters

```python
regex_matches = True
rindex = 0
sindex = 0
reg = ""
string = ""

while regex_matches and rindex < len(reg) and sindex < len(string):
    if reg[rindex] == '*':
        preceding_char = string[sindex - 1]
        lookahead_char = reg[rindex + 1]
        if preceding_char != lookahead_char and preceding_char != '.':
            while string[sindex] == preceding_char:
                sindex += 1
            rindex += 1
        elif preceding_char == lookahead_char and preceding_char != '.':
            while string[sindex:] != reg[rindex:] and sindex < len(string):  # doesn't work if regex pattern begins
                # afterward. Probably require a recursive solution so there's a exploration of valid paths here
                sindex += 1
            if sindex >= len(string):
                regex_matches = False
        else:
            pass  # this is probably actually the same case as the above.
            
    elif reg[rindex] == string[sindex] or reg[rindex] == '.':
        rindex += 1
        sindex += 1
    else:
        regex_matches= False 
        
```

---

# Problem 31 Edit Distance

You are allowed to do character insertions, deletions and substitutions. The edit distance is the minimum of those
three operations in order to get from one string to another string.

The first thing to think about is the converse of the minimum edits -- we need to figure out the maximum number
of elements we can keep the same. In order to do this, you can calculate the intersection of the two strings.

Let n be the length of string 1 and m be the length of string 2. The naive implementation of the intersection is
O(m + n), where you iterate through string 1 and create a HashMap of letters, which is O(1) space. Then iterate through
string 2 and add all collisions to another HashMap, O(1) space. 

Assume that input are all lower case. For interview, ask if upper-case exists and if they are considered distinct.
Assuming, also, ASCII, and English.

You can then create an array of None's of length m. Iterate through the destination and copy every collision with
intersection_set over back to the source_edit. Create a queue and ensure that the letters appear in the proper order 
and are of the proper quantity. Re-set those that do not meet those to None. Then just count Nones and you will know
how many edits are required.

Actually... that doesnt' deal with deletions well... assumes a few things. This solution is wrong....

Well.. actually to the actually, you know the amount of insertions or deletions that must occur due to the delta in 
lengths. Any other changes after that must be replace operations. 

# Problem 32

Suppose you are given a table of currency exchange rates, represented as a 2D array. Deterrmine if there is a possible
arbitrage; that is, whether there is some sequence of trades you can make, starting with some amount A of any currency,
so that you can end up with some amount greater than A of that currency. No transaction costs and you can trade
fractional quantities.

Graph alg, TODO

# Problem 38 N Queens

You have an N by N board. Write a function that, given N, returns the number of possible arrangements of the board
where N queens can be placed on the board without threatening each other, i.e., no two queens share the same row,
column or diagonal.

Given the chessboard, a piece placed down locks down all spots horizontally, vertically and diagonally (in both 
directions) from it. there is no difference between a queen placed on spot x, y in the 1st step as opposed to the 3rd
step (if you are considering each step a single queen placement).

If you were brute-forcing the solution, you might place a queen on an arbitrary square, block off any squares that she
threatens as unavailable, and then iteratively go through the available squares in this manner. Add one to the global
count variable once all squares are either occupied or threatened. Also keep track of the arrangement. Reduce your
count by the number of duplicates.

Using an example of an 8x8 square, you start off with 64 possibilities that you have to iterate through, and the max
you can block off in the first iteration is 4x7, 28. That leaves you with 36 possibilities (N-2)^2.
It'll approximate to something O(N^(x > 2)).

With that approach, if you imagine the recursive branching as a tree, then you have multiple duplications of effort.

# Problem 40

Given array of ints where every integer occurs three times except for one integer, which only occurs once, find and 
return the non-duplicated integer.

Do this in O(N) time and O(1) space.

Without the O(1) space constraint, you would just go through in a single pass with a Counter object and return the 
key where the value was only 1. 

If you knew the missing number was < 3, you could sum up and then modulo
