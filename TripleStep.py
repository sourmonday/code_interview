

def triple_step(steps_count, memo_dict):
    if steps_count in memo_dict.keys():
        return memo_dict[steps_count]
    if steps_count < 0:
        return 0
    if steps_count == 0:
        return 1
    result =  triple_step(steps_count - 3, memo_dict=memo_dict) + triple_step(steps_count - 2, memo_dict=memo_dict) + \
              triple_step(steps_count - 1, memo_dict=memo_dict)
    memo_dict[steps_count] = result
    return result


if __name__ == '__main__':
    print(triple_step(5, dict()))
