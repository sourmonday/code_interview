### 17.13 Re-space

<b>Given:</b> a string, which are sentences with all punctuation and spaces removed, and a dictionary (represented as a list of
strings
The string is composed of words, most of which are in the dictionary. Some are not.

<b>Show:</b> an algorithm that can un-concatenate the document in a way that minimizes the number of unrecognized characters

##### ---- My Solution Notes ----

The document is one string [asldkfjadslkf;jasdlfkjasd;lfk] of arbitrary length. However, you can use meta-data to help
you in your approach.

Assuming that your document is in the English language, you can do an initial pass to look for the most common  words 
with the most common lengths. Convert your list of dictionary words into a HashMap (set in Python) and then split your 
string into n/x strings of x character length (n representing the total document-string length). Check the n/x strings
against the set of dictionary words, then shift-right x times to see what words are caught. These words can then be
"marked off" and processing continued on un-processed buckets of the string. This could continue for other character
lengths according to frequency character data.

<b>Example:</b>
jesslookedjustliketimherbrother

Let's say 3 is the most frequent.

```
jes slo ook edj ust lik eti mhe rbr oth er
ess loo ked jus tli ket imh erb rot her
```

her and loo would be recognized... we can see from this example that this approach could potentially remove small
portions from larger words and decrease the efficiency/accuracy of the approach. You may instead want to take a "greedy"
approach and look for the longest words instead.

Start from the longest English word, let's say it's 21-letters long and process with 21 passes. O(21*n) -> O(n)

Re-presenting your dictionary as a trie, you could read from the beginning of the document-string and search for matches
in the trie. Once a word doesn't match, you "drop" the first character of the un-processed string and start processing
from the n+1 character. If a string matched up until a point, take the last point where a full word was recognized.

<b>Example:</b>
```
jesslookedjustliketimherbrother
```

At some point, jessl would fail to find a word in the trie. j is dropped and the string is considered starting from ess
Same thing happens until the first 4 characters are dropped and lookedjustliketim... is the considered string, 
whereupon lookedj will fail to find a word. looked is the last matching word collision and it will be marked as a 
recognized character.

This would take O(c) time where c is the amount of characters in the doc-string

### Notes from Reviewing Answers

My analysis on runtime was flawed. Because of the branching nature of doing this recursively, It ends up being an
O(2^n). Optimize these types of problems with memoization / caching.

I also don't understand how to truly call out an algorithm as O(2^n) runtime. (TODO)

---

### 17.14 Smallest K

<b>Given:</b> an array

<b>Show:</b> an algorithm to find the smallest K numbers in an array

##### ---- My Solution Notes ----

This is reminiscent of Quicksort and Searching. The element X selected per pass of quick-sort is guaranteed to be locked
into its final location. You are searching for the element Y, which is the X selected in some arbitrary pass, to be the
Kth number in the array.

Let's say that the array is represented as a Python list and that shallow copies of values are accurate representations
of the numbers (i.e., copying the number 1 from a[0] to a[1] is accurate). This gives us O(1) access to arbitrary 
elements. 


<b>Given:</b> Python list, array, with x elements ranging from negative infinity to positive infinity

1. Select random array index, i, where 0 <= i < x
1. Quick-sort pass
1. When the pass terminates, the number originally at array[i] should be at its location array[j]
1. If j < K, select a random element in the index elements j < new_i < x and sort array[j + 1] to array[x]
1. If j > K, select a random element in the index elements 0 < new_i < j and sort array[0] to array[j - 1]
1. If j == K, terminate and return array[0:j]

Quicksort is O(n^2), but is on average O(n log n) with random element selection. As you are not completely sorting the
array, average performance probably has some tighter bound than O(n log n). 

### Notes from Reviewing Answers

Apparently this is a well known algorithm known as Selection Rank. This has constant time complexity.

---

### 17.15 Longest Word

<b>Given:</b> a list of words, write a program to find the longest word made of other words in the list.

##### ---- My Solution Notes ----

Assuming the solution here means that the final answer must be completely composed of words, without leftover
characters, in the list.

Example input:
```
cat, banana, dog, nana, walk, walker, dogwalker
```

1. Start sorting the words in the list of words by word length: O(n log n) step
    ```
    cat, dog, nana, walk, banana, walker, dogwalker
    ```
1. Read words into a trie, keeping track of if the word starts with an element already in the trie. Because the words 
are read-in in sorted order (smallest first), you are guaranteed to find any component words. O(nc)
    ```
    walker: (walk)
    dogwalker: (dog)
    ```
1. For the elements that have components, search for the remaining component within the trie. In the example, only
dogwalker remains. O(c)
1. Of your remaining results, select the longest length string. If the data structure is stable, you should be able to
just select data_struct[x], where x is the last element for O(1) time. Otherwise, O(n log n) sort is required.

In terms of time complexity, the question is whether your list is large such that n dominates. If so, then the constant
c goes away as there is a finite limit to character count in words. Algorithmic complexity, then, is O(n log n)

---

### 17.16 The Masseuse

<b>Given:</b> A sequence of back-to-back appointment requests, all in multiples of 15 minutes, none overlapping, find
the optimal, considered the higest total booked minutes, set that the can be honored.
<b>Given:</b> 15 minute breaks need to be built-in between appointments.

It's not really given the length of an appointment, but it looks like it's 45 minutes.

##### ---- My Solution Notes ----

Example:

```
{30, 15, 60, 75, 45, 15, 15, 45}
```

* The numbers in the sequence are the appointment LENGTHS
* These being adjacent requests, accepting any appointment means that the next appointment shall not be accepted.
* If you look at the edge, being the last appointment, you will generally accept that appointment unless appointment
n-1 is greater than appointment n. 
* What if you looked at the amount of appointment waste if you accepted the prior appointment? There is some action
around the 0 points, but nothing that indicates a hard and fast rule...
   ```
   {NULL, 0, 45, 60, 30, 0, 0, 30}
   ```
* You must consider appointments as units of 3. Accepting an appointment means you did not accept appointment n-1 and 
cannot accept appointment n+1. If you evaluate appointments based on net time in units of 3, 2 for the edges
    ```
    {15, -75, -30, -30, -45, -45, -45, 30}
    ```
* What if you considered appointments as units of 5. Building on the previous, not accepting the n-1 and n+1 means you
could accept n-2 and n+2
    ```
    {75, 0, 45, 0, 30, 75, 0, 45}
    ```
* The previous 2 have significant flaws if we were to then take a greedy approach and select the "highest" value


What if you were to take from a base case approach?
* In the case where the appointment sequence has only 2 elements,  you select the largest element.
```
{x0, x1}  # Select x such that x is the larger element
```
* In the case where you have 3, 
```
{x0, x1, x2}  # Select x1 if x1 > x0 + x2, else the combo is x0, x2
```
* In the case of 4
```
{x0, x1, x2, x3}  # Available combinations are x0, x2; x0, x3; and x1, x3
```
* In the case of 5
```
{x0, x1, x2, x3, x4}  # Available combinations are x0, x2, x4; x0, x3; x1, x3; and x1, x4
```

By brute-forcing it, you can solve it in combinatorial (???, I think) time: O(n^n). Consider all possible combinations
and then sort by highest number of booked minutes.

If you start by considering n = 3, combinations, and therefore calculations, repeat themselves. 

##### After Looking at Hints

<b>Main Insights</b>
* You will never reject three appointments in a row
* If you accept appointment n, you will either accept n+2 or n+3

From these insights, here is an iterative solution:

For each pair of appointments, consider whether x(n) + max(x(n+2), x(n+3)) is greater than x(n+1) + max(x(n+3), x(n+4)).
If equal, select the max between the two.


---

### 17.17 Multi Search

<b>Given:</b> string, b, and an array, T, of smaller strings

Design a method to search b for each small string in T

##### ---- My Solution Notes ----

Brute Force method:

For string in T of size x, divide into buckets and shift right x times in order to search for a matching sub-string. 
If you use a Python set, you can just add each element to the set and use a T-string in set query.

Speaking of Python facilities:

```python
T = set()  # original T with smaller strings
b_string = ""  # original longer string

final_substrings0 = set(filter(lambda substring: substring in b_string, T))

# or

final_substrings1 = set()

for substring in T:
    if substring in b_string:
        final_substrings1.add(substring)
        
```

This is probably in the sub-string class of problems...

You could build a Trie with the strings in T, then search for matches as you rotate through the larger string, b. For
each letter, you either search starting a new word at that letter or continue your search from an already applicable 
search. 

You could move forward to confirm whether or not the search terminates successfully upon character matches from n
and then backtrack to n + 1 upon termination of that check.

Any strings that you found successfully can have a unique success code of some sort appended to the end. At the end
of the string, b, you can iterate through your list of strings in T to check for the success codes.

Roughly:
```python
from collections.abc import Iterable


class Trie:
    def __init__(self):
        raise NotImplementedError
        
    def add(self):
        raise NotImplementedError
        
    def search(self, string)->bool:
        raise NotImplementedError
        
    def mark_found(self, string):
        raise NotImplementedError
        
    def return_all_found(self)->list:
        raise NotImplementedError


class BuildTrie:
    def __init__(self, iter: Iterable):
        self.trie = self.__build(iter)
        
    def __build(self, iter: Iterable)->Trie:
        t = Trie()
        for item in iter:
            t.add(item)
        return t


def solve_problem(T, b):
    trie_strings = BuildTrie(T).trie
    for index, _ in enumerate(b):
        if trie_strings.search(b[index]):
            trie_strings.mark_found(b[index])  # this here requires tweaking to fulfill the two situations of starting @
                # and continuing from
    return trie_strings.return_all_found()
    
```

Time complexity: O(b + t_c), t_c is longest character length of substring in T (I think?)

---

### 17.18 Shortest Super-sequence

<b>Given:</b> Two arrays, shorter (all distinct elements) and longer

Find the shortest subarray in the longer array that contains all elements in the shorter array

<b>Example</b>

```
# Input
{1, 5, 9} | {7, 5, 9, 0, 2, 1, 3, 5, 7, 9, 1, 1, 5, 8, 8, 9, 7}

# Output
[7, 10]  # That is,longer[7:10] contains 1, 5, and 9 and is the shortest sub-array where this is true.
```

##### ---- My Solution Notes ----

There are x distinct elements within the array, shorter. For each of those values, you need to find the shortest run
in longer where all values are contained. Values that are not of these values do not matter. For the example input,
this can reduce to:

```
# Shorter
{A, B, C}

# Longer
{0, B, C, 0, 0, A, 0, B, 0, C, A, A, B, 0, 0, C, 0}
```

1. Solution property 1: you want to reduce the amounts of zeroes of your solution. This needs to be the minimum amount
    1. Note re: Property 1, this isn't exactly true. You can minimize the 0s and still have the wrong answer. Example 
    being AAAAAAAAAAAAAABC. There are no zeroes here but it is still a long sequence.
1. Solution property 2: all values in Shorter must be contained in the solution
1. For each value, it can either be part of a run or the start of a new run. For each value in longer that is an element
A, B, or C (or any other value for more values), you can treat it as either the beginning or the center
    1. A center solution is reminiscent of the palindrome problems. You can treat and then expand. See if the new
    sub-array meets Property 2. You can keep track of the length as part of the expand. You might need to fix if the new
    sub-array meets Property 2 in the case where one element on either end of the sub-array is a zero.
    
---

### 17.19 Missing Two

<b>Given:</b> You are given an array with all the numbers from 1 to N appearing exactly once, except for one number that
is missing. 

How can you find the missing number in O(n) time and O(1) space? What if there were two numbers missing?

##### ---- My Solution Notes ----

You know that the formula for the sum of numbers for 1 to N is:

```
n(n+1) / 2
```

Therefore, go through a pass of 1 through N once in order to calculate the sum of the array. When you subtract from the
sum total ```n(n+1) / 2```, you will find the missing number. O(1) space for the expected_sum variable, either INT or 
LONG, LONG_LONG types. O(N) time because only one pass.

Using this solution for an array missing two elements, you would obtain the sum of the two missing numbers. This sum
means that the missing numbers must be two numbers that are less than this result (numbers start from 1, not 0).

1st pass: difference is Z.

The two numbers x, y that you are seeking obey the property ```1 <= x <= y < Z```.

You can track the expected sum as you iterate through the array again. Constant calculation of ```n(n+1) / 2``` over 
Z operations, which in the worst case is N, means this second pass is an O(N) operation.

2nd pass: as you iterate through, keep track of expected_sum to find the first missing number. Once you find the first
missing number, x, terminate. Your second missing number is Z - x = y

Two variables are required. O(N) time achieved.

---

### 17.20 Continuous Median

<b>Given:</b> a continuous stream of random numbers

Create a method that finds and maintains the median values as numbers are created.

##### ---- My Solution Notes ----

Initial solution, you are given a stream. In order to find the median value, ensuring that your array is sorted and 
then querying the middle number(s) based on array_size is a very natural way to do it. Insertion sort would be the
typical solution. While insertion sort, in general has O(n^2) performance, insertion in an already sorted array gives
you O(n) performance on the insertion. 

Due to the nature of values being passed on as a stream of values, O(n) amortized cost with insertion sort is the 
bound, unless there is some O(n) total complexity solution, which would be amortized down to O(1)

You might be able to achieve O(1) time with a HashMap. 

You can keep track of the value_stream_min and value_stream_max in order to know how many int-size memory spaces you
need to reserve for your HashMap. You can then keep track of the current median-statistic and then evaluate how the
calculation needs to shift based on which side of the see-saw the new value from the value stream falls. Insertion
into a HashMap is constant time. 

If unique values are not guaranteed by the stream, you require an additional data structure to keep track of the
number of each value that is turned on by the HashMap. 

In terms of space requirements, this requires O(n) space unless you are guaranteed some arbitrary max on the values 
coming from the value stream, in which case, over time, O(n) becomes O(1)

### Looking at Answers

Consider two Heaps

----

### 17.21 Volume of Histogram

<b>Given:</b> an array of numbers representing a histogram

Design an algorithm that shows how much water the histogram would hold if you poured water over it.

<b>Example:</b>
```
Input: {0, 0, 4, 0, 0, 6, 0, 0, 3, 0, 5, 0, 1, 0, 0, 0}
Output: 26
Diagram found on page 189 of Cracking the Coding Interview
```

##### ---- My Solution Notes ----

What you essentially have is a 2-d grid that has three different states, air, structure, and water. In the context of
this problem, that reduces down to two states: water and not water.

Water needs to be constrained by structures, but cannot be constrained by air. So with that, we're back to 3 states.

A brute force method would be to create a 2-dimensional array representing the histogram. All values could be
initialized to "air" and the bar for each non-zero value from the array would be set to structure on a first pass.

A second pass would be required to set any air spaces that are constrained by structures to water, counting as  you go.

Two passes makes this O(n) time complexity with O(m * n) space requirements, where m is the largest value of the array


The problem could also be accomplished by keeping track of states of water accumulation. You can definitely get this
down to O(n) time with O(1) space. This is your best theoretical time-space complexity.

As you run through the array, these are the potential states of water accumulation:
1. 0 water, values are 0 and we are unconstrained on one side by any structure.
1. 0 water, but a structure has just been set.
1. z bar(s) of x width and y height, constrained by structures
    1. there are two structures with one taller than the other, nothing else in-between. Water is accumulated by taking
    the lower height and multiplying it by the amount of 0's (width) in between the two structures. If the structures
    are of equal height, this still applies
    1. there are two structures with either equal height or unequal height, but there is one or are many structures 
    in-between that are of shorter height.
    
To deal with property 1, you can simply strip 0-values from both ends of the array

```
Input: {4, 0, 0, 6, 0, 0, 3, 0, 5, 0, 1}
```

Values[0] and [n] (values at both ends) meet property 2, only in-between do you start to accumulate water.

For each bar in between Values[1:n-1], you need to figure out how much to fill the bar. By always filling every
bar that is zero to the lower of the structure constraints, you obtain this initial array of water values:

```
Water Values: {0, 4, 4, 0, 3, 3, 0, 3, 0, 1, 0}
```

Expansion from the center is O(n) per n value, making it O(n^2) overall. However, there are repeated calculations. 

Instead of an expand from center operation, do a find_next_structure operation for each structure you find and set
all zeroes in between to the min(structure1, structure2) in your water_values array.

You now have to deal with Property(3.ii). 

Originally, I thought that you could go a one-level deep recursive call to take off structure heights by taking the
minimum of flanking values. (If that wasn't clear, it's okay.) That lead to inaccurate results because of the
inconsistency of using a min call. However, recursion can be a valid approach.

You accumulate water levels based on the lowest height global structure. For all zeroes, you add the lowest height
amount of water and then get rid of the structure. If the structure was at the end and is no longer constraining a 
space, you no longer add water to that space. Do this either iteratively or recursively until there are no spaces
that are eligible for adding watter.

---

### 17.22 Word Transformer

<b>Given:</b> two words of equal length that are in a dictionary

Write a method to transform one word into another word by changing by changing only one letter at a time. The new word
you get in each step must be in the dictionary.

##### ---- My Solution Notes ----

Each word needs to recursively transform into another word until you reach your final path. You can only take
one-letter steps and each of those one-letter steps needs to end in another valid word.

One way you could do this is with a graph algorithm. 

Each node would only be adjacent to another node that meets both properties (valid word & one letter off). You would
then do a breadth-first search from your initial word/node to look for your final word/node and print the path thereof.

The question, then, is how would you generate the graph such that the property holds. And if you can do that, just do 
that process for this one specific case in order to obtain the answer instead of generating the entire graph.

To solve the sub-problem of one-letter off search, that is to merely find all words in the dictionary that are one-off
from an arbitrary point, can we create some kind of a hashing algorithm that matches those with similar letters.

* You know that letter lengths must be the same, so you can bucket any searches within all word lengths that are 
of the same length.
* You know that letters must match in all positions but one. That is, there must be one and only one letter that is
different AND that all other letters match and the order of that match matters.

Probably roughly
```python
original_dictionary = set()
initial_word = ""

assert initial_word in original_dictionary

# Lengths are the same
candidate_bucket = set(filter(lambda word: len(word) == len(initial_word), original_dictionary))
```

Probably roughly
```python
def generate_oneoff(initial_word, candidate_set):
    oneoff_set = set()
    for char_index, _ in enumerate(initial_word):
        subset = set(filter(lambda oneoff: (oneoff[:char_index]+oneoff[char_index+1:]) == \
            (initial_word[:char_index]+initial_word[char_index+1:]), candidate_set
        ))
        oneoff_set = oneoff_set.union(subset)
    return oneoff_set

```

You also need to prune the results returned the above method in order to ensure you aren't hitting the same node
over and over again (loops). You can do that with a class variable HashMap to keep track of nodes/words that you have
already seen. Eventually, you either terminate with no more available nodes or the path you need to follow.

Without Python functional programming facilities, the solution might look something like:

```python
def generate_hash(word):
    _hash_list = list()
    # returns perfect hashes of the word with one letter removed each 
    # for example lard gives you hashes for ard, lrd, lad, lar in a tuple
    return tuple(_hash_list)
    
def match_hash(hash1: tuple, hash2: tuple) -> bool:
    assert len(hash1) is len(hash2)
    for x, y in zip(hash1, hash2):
        if x != y:
            return False
    return True

def generate_oneoff(initial_word, candidate_set):
    matching_hash = generate_hash(initial_word)
    matches = set()
    for word in candidate_set:
        candidate_hash = generate_hash(word)
        if match_hash(candidate_hash, matching_hash):
            matches.add(word)
        
    return matches
```
O(l * n), l is the length of the word

---

### 17.23 Max Black Square

<b>Given:</b> A square matrix where each cell (pixel) is either black or white

Design an algorithm to find the maximum subsquare such that all four borders are filled with black pixels.

##### ---- My Solution Notes ----

In terms of the most optimal theoretical solution, you probably have to read all values in the square matrix. Just
doing one pass of reads bounds you to an O(n^2) solution, n representing a dimension of the square matrix. 

Guess 1:
By relaxing the requirement for the sub-squares to quadrilaterals, you can then later re-constrain your results to 
squares. Then find the largest square of those results. Question, then, is can we solve for quadrilaterals?

A quadrilateral has the following properties:
1. Two sets of parallel lines with some equal length along the same coordinates
1. 4 intersections of those parallel lines to enclose the quadrilateral

Using the square matrix array with structure, matrix[x][y], x is the horizontal axis.

```python
from enum import Enum

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Line:
    def __init__(self, origin: Point, destination: Point):
        self.origin = origin
        self.destination = destination

class Color(Enum):
    White = 0
    Black = 1
    
x = y = 5
matrix = list()  # 2-d array
horizontals = list()

for row in range(x):
    column = 0
    while column < y:
        if matrix[row][column] is Color.Black:
            origin = Point(row, column)
            while matrix[row][column] is Color.Black or column >= y:
                column += 1
            destination = Point(row, column - 1)
            horizontals.append(Line(origin, destination))
        else:
            column += 1
            
# do the same as above for verticals
```

Once you have all the lines that exist within the matrix, you can then search for parallel segments and then filter
for the subset of those that match having four intersections.

You then filter out those that are squares. Then you return the max area of the squares.

You then dive into the objects in order to get the points that constrain the square. You might have had to create
new objects if some lines extended past their point of intersection. You then return the answer that the question is
looking for, which are the bounds of the sub-square.

This looks like an O(n^2) solution as running through the 2 dimensions twice is a 2 * n^2 operation. The following
sub-problems are then O(n) filters.

---

### 17.24 Max Sub-matrix

<b>Given:</b> a NxN matrix of positive and negative integers

Find the submatrix with the largest possible sum.

##### ---- My Solution Notes ----

First, consider a recursive approach. What's the smallest case?

For 1x1, it must be itself. 

For 2x2, the largest could be either each individual cell, the 1x2 on either of horizontals, the 2x1 of the verticals,
or the entire matrix.

For the 3x3, you have to consider the largest of the previous case, the previously calculated horizontals + the added
corresponding elements, the previously calculated verticals + the added corresponding elements, and then the new
sub matrices expanding both vertically and horizontally from the added element. All of these have components that 
were already previously calculated. 

Basically, any previously calculated submatrix that had an edge that matched up with the old edge-of-the-world would
then add submatrices based on previous calculations and added elements. You then assign 
```current_max = max(set(current_max).union(set(new_submatrices)))```

```python
# TODO: expand on this solution prior to looking at hints and answers

```

---

### 17.25 Word Rectangle

<b>Given:</b> list of a million words

Design an algorithm to create the largest possible rectangle of letters such that every row forms a word (reading left
to right) and every column forms a word (reading top to bottom). The words need not be chosen consecutively from the
list, but all rows must be the same length and all columns must be the same height.

##### ---- My Solution Notes ----

```python
# TODO
```

---

### 17.26 Sparse Similarity

Similarity of two documents (each with distinct words) is defined to be the size of the intersection divided by the 
size of the union. For example, if the documents consist of integers, the similarly of {1, 5, 3} and {1, 7, 2, 3} is
0.4, because the intersection has size 2 and the union has size 5.

There is a long list of documents, each with a distinct values and each with an associated ID, where the similarity is
believed to be "sparse."

You may assume each document to be represented by an array of distinct integers

Print only pairs with similarity greater than 0. Empty documents should not be printed at all.

##### ---- My Solution Notes ----

```
Input:
    13: {14, 15, 100, 9, 3}
    16: {32, 1, 9, 3, 5}
    19: {15, 29, 2, 6, 8, 7}
    24: {7, 10}
Output:
    ID1, ID2 : SIMILARITY
    13, 19 : 0.1
    13, 16 : 0.25
    19, 24 : 0.1428...
```

If you were to compare each document to every other document, you will have a O(n!) solution. There is a lot of wasted
effort when most documents should return a similarity index of 0.

The first easy thing to do is to prune out all empty documents:

```python
documents = set()  # set of all documents, which are represented by sets
valid_documents = set(filter(lambda x: len(x) > 0, documents))
```

The important reduction is to never compare any pairs of documents that will result in a 0 similarity, that is there 
is no intersection. If you want to figure out which set has no intersections with any of the other sets, you have
to do a compare with all other elements, constraining you to a O(n!) operation. 

If you go through and create a superset_of_others per document, you have a n*(n-1) operation, which reduces to O(n^2).

With the tuple (document_set, superset_of_others), you do a O(n) reduction of those that do not have any intersections
with any others

At this point, you will be left with those documents that have intersections with at least one other document in the
set of all documents. You still might have a lot of useless compares as there are probably cases where the intersection
is only between two specific documents in this reduced superset of ten documents, for example. That's still eight
wasted compares.

... ???
